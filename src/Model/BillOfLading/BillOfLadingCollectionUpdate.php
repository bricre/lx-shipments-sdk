<?php

namespace LogisticsX\Shipments\Model\BillOfLading;

use OpenAPI\Runtime\AbstractModel;

class BillOfLadingCollectionUpdate extends AbstractModel
{
    /**
     * @var int|null
     */
    public $clientId = null;

    /**
     * @var string|null
     */
    public $clientCode = null;

    /**
     * @var string
     */
    public $status = 'PENDING';

    /**
     * @var string|null
     */
    public $customsClearanceType = null;
}
