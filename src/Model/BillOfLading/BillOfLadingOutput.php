<?php

namespace LogisticsX\Shipments\Model\BillOfLading;

use OpenAPI\Runtime\AbstractModel;

class BillOfLadingOutput extends AbstractModel
{
    /**
     * @var string
     */
    public $id = null;

    /**
     * @var string
     */
    public $uuid = null;

    /**
     * @var int|null
     */
    public $clientId = null;

    /**
     * @var string|null
     */
    public $clientCode = null;

    /**
     * @var string
     */
    public $reference = null;

    /**
     * @var string|null
     */
    public $customsEntry = null;

    /**
     * @var string|null
     */
    public $ucn = null;

    /**
     * @var string|null
     */
    public $note = null;

    /**
     * @var string|null
     */
    public $exporterName = null;

    /**
     * @var string|null
     */
    public $exporterAddress = null;

    /**
     * @var string|null
     */
    public $importerName = null;

    /**
     * @var string|null
     */
    public $importerAddress = null;

    /**
     * @var string|null
     */
    public $importerVat = null;

    /**
     * @var string|null
     */
    public $importerEori = null;

    /**
     * @var string
     */
    public $createTime = null;

    /**
     * @var string|null
     */
    public $updateTime = null;

    /**
     * @var string|null
     */
    public $customsStatus = null;

    /**
     * @var float
     */
    public $customsFee = null;

    /**
     * @var bool
     */
    public $customsFeePaid = null;

    /**
     * @var string
     */
    public $status = 'PENDING';

    /**
     * @var string|null
     */
    public $customsClearanceType = null;

    /**
     * @var bool
     */
    public $isForPva = null;

    /**
     * @var string
     */
    public $termsOfSales = 'DDP';

    public $masterWaybill = null;

    public $billOfLadingDelivery = null;

    public $container = null;

    /**
     * @var \LogisticsX\Shipments\Model\Consignment\BillOfLadingOutput[]
     */
    public $consignments = null;

    public $customsBroker = null;

    /**
     * @var int
     */
    public $cartonCount = null;

    /**
     * @var float
     */
    public $totalVolume = null;

    /**
     * @var int
     */
    public $totalWeight = null;

    /**
     * @var int
     */
    public $consignmentCartonCount = null;

    /**
     * @var int
     */
    public $consignmentCount = null;

    /**
     * @var
     * \LogisticsX\Shipments\Model\ConsignmentCartonProductSummary\BillOfLadingOutput
     */
    public $consignmentCartonProductSummary = null;
}
