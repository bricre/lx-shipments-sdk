<?php

namespace LogisticsX\Shipments\Model\BillOfLading;

use OpenAPI\Runtime\AbstractModel;

class BillOfLadingCollection extends AbstractModel
{
    /**
     * @var string
     */
    public $id = null;

    /**
     * @var string
     */
    public $uuid = null;

    /**
     * @var int|null
     */
    public $clientId = null;

    /**
     * @var string|null
     */
    public $clientCode = null;

    /**
     * @var string
     */
    public $reference = null;

    /**
     * @var string|null
     */
    public $customsEntry = null;

    /**
     * @var string|null
     */
    public $ucn = null;

    /**
     * @var string|null
     */
    public $exporterName = null;

    /**
     * @var string|null
     */
    public $exporterAddress = null;

    /**
     * @var string|null
     */
    public $importerName = null;

    /**
     * @var string|null
     */
    public $importerAddress = null;

    /**
     * @var string|null
     */
    public $importerVat = null;

    /**
     * @var string|null
     */
    public $importerEori = null;

    /**
     * @var string
     */
    public $createTime = null;

    /**
     * @var string|null
     */
    public $updateTime = null;

    /**
     * @var string|null
     */
    public $customsStatus = null;

    /**
     * @var float
     */
    public $customsFee = null;

    /**
     * @var bool
     */
    public $customsFeePaid = null;

    /**
     * @var string
     */
    public $status = 'PENDING';

    /**
     * @var string|null
     */
    public $customsClearanceType = null;

    /**
     * @var bool
     */
    public $isForPva = null;

    public $masterWaybill = null;

    public $container = null;

    /**
     * @var string[]|
     */
    public $consignments = null;

    /**
     * @var float
     */
    public $totalVolume = null;

    /**
     * @var int
     */
    public $totalWeight = null;

    /**
     * @var int
     */
    public $consignmentCartonCount = null;

    /**
     * @var int
     */
    public $consignmentCount = null;
}
