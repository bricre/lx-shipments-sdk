<?php

namespace LogisticsX\Shipments\Model\BillOfLading;

use OpenAPI\Runtime\AbstractModel;

class BillOfLadingInput extends AbstractModel
{
    /**
     * @var string
     */
    public $uuid = null;

    /**
     * @var int|null
     */
    public $clientId = null;

    /**
     * @var string
     */
    public $reference = null;

    /**
     * @var string|null
     */
    public $customsEntry = null;

    /**
     * @var string|null
     */
    public $ucn = null;

    /**
     * @var string|null
     */
    public $note = null;

    /**
     * @var string|null
     */
    public $exporterName = null;

    /**
     * @var string|null
     */
    public $exporterAddress = null;

    /**
     * @var string|null
     */
    public $importerName = null;

    /**
     * @var string|null
     */
    public $importerAddress = null;

    /**
     * @var string|null
     */
    public $importerVat = null;

    /**
     * @var string|null
     */
    public $importerEori = null;

    /**
     * @var string|null
     */
    public $customsStatus = null;

    /**
     * @var bool
     */
    public $customsFeePaid = null;

    /**
     * @var string
     */
    public $status = 'PENDING';

    /**
     * @var string|null
     */
    public $customsClearanceType = null;

    /**
     * @var bool
     */
    public $isForPva = null;

    /**
     * @var string
     */
    public $termsOfSales = 'DDP';

    /**
     * @var string|null
     */
    public $masterWaybill = null;

    /**
     * @var string|null
     */
    public $billOfLadingDelivery = null;

    /**
     * @var string|null
     */
    public $container = null;

    /**
     * @var string[]|
     */
    public $consignments = null;

    /**
     * @var string|null
     */
    public $customsBroker = null;
}
