<?php

namespace LogisticsX\Shipments\Model\BillOfLading;

use OpenAPI\Runtime\AbstractModel;

class BillOfLadingCollectionInput extends AbstractModel
{
    /**
     * @var string|null
     */
    public $customsStatus = null;

    /**
     * @var string
     */
    public $status = 'PENDING';
}
