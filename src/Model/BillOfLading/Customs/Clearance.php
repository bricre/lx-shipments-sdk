<?php

namespace LogisticsX\Shipments\Model\BillOfLading\Customs;

use OpenAPI\Runtime\AbstractModel;

class Clearance extends AbstractModel
{
    /**
     * @var string
     */
    public $id = null;

    /**
     * @var string
     */
    public $uuid = null;

    /**
     * @var string
     */
    public $reference = null;

    /**
     * @var string|null
     */
    public $ucn = null;

    /**
     * @var string|null
     */
    public $importerVat = null;

    /**
     * @var string|null
     */
    public $importerEori = null;

    /**
     * @var string|null
     */
    public $customsStatus = null;

    /**
     * @var float
     */
    public $customsFee = null;

    /**
     * @var bool
     */
    public $customsFeePaid = null;

    /**
     * @var string
     */
    public $status = 'PENDING';

    /**
     * @var string|null
     */
    public $customsClearanceType = null;
}
