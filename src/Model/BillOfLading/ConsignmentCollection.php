<?php

namespace LogisticsX\Shipments\Model\BillOfLading;

use OpenAPI\Runtime\AbstractModel;

class ConsignmentCollection extends AbstractModel
{
    /**
     * @var string
     */
    public $id = null;

    /**
     * @var string
     */
    public $uuid = null;

    /**
     * @var string
     */
    public $reference = null;
}
