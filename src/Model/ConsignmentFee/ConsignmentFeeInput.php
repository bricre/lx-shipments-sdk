<?php

namespace LogisticsX\Shipments\Model\ConsignmentFee;

use OpenAPI\Runtime\AbstractModel;

class ConsignmentFeeInput extends AbstractModel
{
    /**
     * @var string
     */
    public $amount = null;

    /**
     * @var string|null
     */
    public $shipmentFeeTypeName = null;

    /**
     * @var string|null
     */
    public $consignment = null;
}
