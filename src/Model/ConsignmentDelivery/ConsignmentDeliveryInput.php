<?php

namespace LogisticsX\Shipments\Model\ConsignmentDelivery;

use OpenAPI\Runtime\AbstractModel;

class ConsignmentDeliveryInput extends AbstractModel
{
    /**
     * @var string|null
     */
    public $deliveryReference = null;

    /**
     * @var string|null
     */
    public $consignment = null;
}
