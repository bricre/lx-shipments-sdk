<?php

namespace LogisticsX\Shipments\Model\ConsignmentDelivery\Customs;

use OpenAPI\Runtime\AbstractModel;

class Clearance extends AbstractModel
{
    /**
     * @var string|null
     */
    public $deliveryReference = null;
}
