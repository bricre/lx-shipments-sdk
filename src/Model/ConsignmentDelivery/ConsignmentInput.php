<?php

namespace LogisticsX\Shipments\Model\ConsignmentDelivery;

use OpenAPI\Runtime\AbstractModel;

class ConsignmentInput extends AbstractModel
{
    /**
     * @var string|null
     */
    public $deliveryReference = null;
}
