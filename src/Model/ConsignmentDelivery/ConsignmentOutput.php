<?php

namespace LogisticsX\Shipments\Model\ConsignmentDelivery;

use OpenAPI\Runtime\AbstractModel;

class ConsignmentOutput extends AbstractModel
{
    /**
     * @var string|null
     */
    public $deliveryReference = null;

    public $consignmentDeliveryError = null;
}
