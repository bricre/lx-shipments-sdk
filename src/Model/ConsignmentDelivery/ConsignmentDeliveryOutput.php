<?php

namespace LogisticsX\Shipments\Model\ConsignmentDelivery;

use OpenAPI\Runtime\AbstractModel;

class ConsignmentDeliveryOutput extends AbstractModel
{
    /**
     * @var string
     */
    public $uuid = null;

    /**
     * @var string|null
     */
    public $deliveryReference = null;

    /**
     * @var string|null
     */
    public $consignment = null;

    /**
     * @var string|null
     */
    public $consignmentDeliveryError = null;
}
