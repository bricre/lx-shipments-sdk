<?php

namespace LogisticsX\Shipments\Model\ConsignmentDelivery;

use OpenAPI\Runtime\AbstractModel;

class ContainerOutput extends AbstractModel
{
    /**
     * @var string|null
     */
    public $deliveryReference = null;
}
