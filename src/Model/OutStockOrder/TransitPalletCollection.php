<?php

namespace LogisticsX\Shipments\Model\OutStockOrder;

use OpenAPI\Runtime\AbstractModel;

/**
 * OutStockOrder.
 */
class TransitPalletCollection extends AbstractModel
{
    /**
     * @var string
     */
    public $uuid = null;

    /**
     * @var string|null
     */
    public $deliveryReference = null;
}
