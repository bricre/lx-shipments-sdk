<?php

namespace LogisticsX\Shipments\Model\OutStockOrder;

use OpenAPI\Runtime\AbstractModel;

/**
 * OutStockOrder.
 */
class OutStockOrderOutput extends AbstractModel
{
    /**
     * @var string
     */
    public $uuid = null;

    /**
     * @var string
     */
    public $reference = null;

    /**
     * @var string
     */
    public $contact = null;

    /**
     * @var string|null
     */
    public $businessName = null;

    /**
     * @var string
     */
    public $addressLine1 = null;

    /**
     * @var string|null
     */
    public $addressLine2 = null;

    /**
     * @var string|null
     */
    public $addressLine3 = null;

    /**
     * @var string|null
     */
    public $city = null;

    /**
     * @var string|null
     */
    public $county = null;

    /**
     * @var string
     */
    public $countryIso = null;

    /**
     * @var string|null
     */
    public $postcode = null;

    /**
     * @var string|null
     */
    public $telephone = null;

    /**
     * @var string|null
     */
    public $email = null;

    /**
     * @var string|null
     */
    public $deliveryReference = null;

    /**
     * @var string
     */
    public $createTime = null;

    /**
     * @var string
     */
    public $updateTime = null;
}
