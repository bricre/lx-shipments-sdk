<?php

namespace LogisticsX\Shipments\Model\MasterWaybill;

use OpenAPI\Runtime\AbstractModel;

class ContainerOutput extends AbstractModel
{
    /**
     * @var string
     */
    public $uuid = null;

    /**
     * @var string
     */
    public $reference = null;

    /**
     * @var string|null
     */
    public $meansOfTransportCode = null;

    /**
     * @var string|null
     */
    public $placeOfDepartureLocationName = null;

    /**
     * @var string|null
     */
    public $placeOfDepartureLocationCode = null;

    /**
     * @var string|null
     */
    public $placeOfDeliveryLocationName = null;

    /**
     * @var string|null
     */
    public $placeOfDeliveryLocationCode = null;

    /**
     * @var string|null
     */
    public $estimatedDateOfDeparture = null;

    /**
     * @var string|null
     */
    public $estimatedDateOfArrival = null;

    /**
     * @var string|null
     */
    public $actualDateOfDeparture = null;

    /**
     * @var string|null
     */
    public $actualDateOfArrival = null;
}
