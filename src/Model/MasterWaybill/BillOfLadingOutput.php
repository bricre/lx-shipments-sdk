<?php

namespace LogisticsX\Shipments\Model\MasterWaybill;

use OpenAPI\Runtime\AbstractModel;

class BillOfLadingOutput extends AbstractModel
{
    /**
     * @var string
     */
    public $uuid = null;

    /**
     * @var string
     */
    public $reference = null;

    /**
     * @var string|null
     */
    public $meansOfTransportCode = null;

    /**
     * @var string|null
     */
    public $senderName = null;

    /**
     * @var string|null
     */
    public $senderAddress = null;

    /**
     * @var string|null
     */
    public $consigneeName = null;

    /**
     * @var string|null
     */
    public $consigneeAddress = null;

    /**
     * @var string|null
     */
    public $notifyPartyName = null;

    /**
     * @var string|null
     */
    public $notifyPartyAddress = null;

    /**
     * @var string|null
     */
    public $placeOfDepartureLocationName = null;

    /**
     * @var string|null
     */
    public $placeOfDepartureLocationCode = null;

    /**
     * @var string|null
     */
    public $placeOfDeliveryLocationName = null;

    /**
     * @var string|null
     */
    public $placeOfDeliveryLocationCode = null;

    /**
     * @var string|null
     */
    public $estimatedDateOfDeparture = null;

    /**
     * @var string|null
     */
    public $estimatedDateOfArrival = null;

    /**
     * @var string|null
     */
    public $actualDateOfDeparture = null;

    /**
     * @var string|null
     */
    public $actualDateOfArrival = null;
}
