<?php

namespace LogisticsX\Shipments\Model\MasterWaybill;

use OpenAPI\Runtime\AbstractModel;

class BillOfLadingCollection extends AbstractModel
{
    /**
     * @var string
     */
    public $reference = null;
}
