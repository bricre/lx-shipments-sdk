<?php

namespace LogisticsX\Shipments\Model\MasterWaybill;

use OpenAPI\Runtime\AbstractModel;

class ContainerCollection extends AbstractModel
{
    /**
     * @var string
     */
    public $reference = null;

    /**
     * @var int|null
     */
    public $logisticsProviderId = null;

    /**
     * @var string|null
     */
    public $logisticsProviderCode = null;

    /**
     * @var string|null
     */
    public $meansOfTransportCode = null;

    /**
     * @var string|null
     */
    public $placeOfDepartureLocationName = null;

    /**
     * @var string|null
     */
    public $placeOfDepartureLocationCode = null;

    /**
     * @var string|null
     */
    public $placeOfDeliveryLocationName = null;

    /**
     * @var string|null
     */
    public $placeOfDeliveryLocationCode = null;

    /**
     * @var string|null
     */
    public $estimatedDateOfDeparture = null;

    /**
     * @var string|null
     */
    public $estimatedDateOfArrival = null;

    /**
     * @var string|null
     */
    public $actualDateOfDeparture = null;

    /**
     * @var string|null
     */
    public $actualDateOfArrival = null;
}
