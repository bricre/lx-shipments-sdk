<?php

namespace LogisticsX\Shipments\Model\MasterWaybill;

use OpenAPI\Runtime\AbstractModel;

class MasterWaybillOutput extends AbstractModel
{
    /**
     * @var string
     */
    public $uuid = null;

    /**
     * @var string
     */
    public $reference = null;

    /**
     * @var int|null
     */
    public $clientId = null;

    /**
     * @var string|null
     */
    public $clientCode = null;

    /**
     * @var string|null
     */
    public $bookingNumber = null;

    /**
     * @var int|null
     */
    public $logisticsProviderId = null;

    /**
     * @var string|null
     */
    public $logisticsProviderCode = null;

    /**
     * @var string|null
     */
    public $meansOfTransportCode = null;

    /**
     * @var string|null
     */
    public $senderName = null;

    /**
     * @var string|null
     */
    public $senderAddress = null;

    /**
     * @var string|null
     */
    public $consigneeName = null;

    /**
     * @var string|null
     */
    public $consigneeAddress = null;

    /**
     * @var string|null
     */
    public $notifyPartyName = null;

    /**
     * @var string|null
     */
    public $notifyPartyAddress = null;

    /**
     * @var string|null
     */
    public $placeOfDepartureLocationName = null;

    /**
     * @var string|null
     */
    public $placeOfDepartureLocationCode = null;

    /**
     * @var string|null
     */
    public $placeOfDeliveryLocationName = null;

    /**
     * @var string|null
     */
    public $placeOfDeliveryLocationCode = null;

    /**
     * @var string|null
     */
    public $estimatedDateOfDeparture = null;

    /**
     * @var string|null
     */
    public $estimatedDateOfArrival = null;

    /**
     * @var string|null
     */
    public $actualDateOfDeparture = null;

    /**
     * @var string|null
     */
    public $actualDateOfArrival = null;

    /**
     * @var string
     */
    public $createTime = null;

    /**
     * @var string
     */
    public $updateTime = null;

    /**
     * @var string|null
     */
    public $note = null;

    /**
     * @var string
     */
    public $type = 'SEA';

    /**
     * @var string|null
     */
    public $status = null;

    /**
     * @var string[]|
     */
    public $consignments = null;

    /**
     * @var string[]|
     */
    public $containers = null;

    /**
     * @var
     * \LogisticsX\Shipments\Model\ConsignmentCartonProductSummary\MasterWaybillOutput
     */
    public $consignmentCartonProductSummary = null;
}
