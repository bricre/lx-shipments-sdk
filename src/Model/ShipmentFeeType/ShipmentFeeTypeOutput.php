<?php

namespace LogisticsX\Shipments\Model\ShipmentFeeType;

use OpenAPI\Runtime\AbstractModel;

class ShipmentFeeTypeOutput extends AbstractModel
{
    /**
     * @var string
     */
    public $name = null;
}
