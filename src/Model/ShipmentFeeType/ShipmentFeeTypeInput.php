<?php

namespace LogisticsX\Shipments\Model\ShipmentFeeType;

use OpenAPI\Runtime\AbstractModel;

class ShipmentFeeTypeInput extends AbstractModel
{
    /**
     * @var string
     */
    public $name = null;
}
