<?php

namespace LogisticsX\Shipments\Model\ConsignmentDeliveryError;

use OpenAPI\Runtime\AbstractModel;

class ConsignmentOutput extends AbstractModel
{
    /**
     * @var string|null
     */
    public $error = null;
}
