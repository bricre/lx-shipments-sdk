<?php

namespace LogisticsX\Shipments\Model\MasterWaybillCollection;

use OpenAPI\Runtime\AbstractModel;

class MasterWaybillCollectionOutput extends AbstractModel
{
    /**
     * @var string[]|
     */
    public $masterWaybills = null;
}
