<?php

namespace LogisticsX\Shipments\Model\MasterWaybillCollection;

use OpenAPI\Runtime\AbstractModel;

class MasterWaybillCollectionUpdate extends AbstractModel
{
    /**
     * @var string[]|
     */
    public $masterWaybills = null;

    /**
     * @var string|null
     */
    public $status = null;
}
