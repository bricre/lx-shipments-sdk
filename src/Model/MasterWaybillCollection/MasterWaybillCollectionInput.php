<?php

namespace LogisticsX\Shipments\Model\MasterWaybillCollection;

use OpenAPI\Runtime\AbstractModel;

class MasterWaybillCollectionInput extends AbstractModel
{
    /**
     * @var string[]|
     */
    public $masterWaybills = null;

    /**
     * @var string|null
     */
    public $clientCode = null;
}
