<?php

namespace LogisticsX\Shipments\Model\Consignment;

use OpenAPI\Runtime\AbstractModel;

class BillOfLadingOutput extends AbstractModel
{
    /**
     * @var \LogisticsX\Shipments\Model\ConsignmentCarton\BillOfLadingOutput[]
     */
    public $consignmentCartons = null;

    public $consignmentDelivery = null;

    public $deliveryService = null;

    /**
     * @var float
     */
    public $totalVolume = null;

    /**
     * @var float
     */
    public $totalWeight = null;

    /**
     * @var
     * \LogisticsX\Shipments\Model\ConsignmentCartonProductSummary\BillOfLadingOutput
     */
    public $consignmentCartonProductSummary = null;

    /**
     * @var string
     */
    public $fullAddress = null;
}
