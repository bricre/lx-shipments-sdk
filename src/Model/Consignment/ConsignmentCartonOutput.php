<?php

namespace LogisticsX\Shipments\Model\Consignment;

use OpenAPI\Runtime\AbstractModel;

class ConsignmentCartonOutput extends AbstractModel
{
    /**
     * @var string
     */
    public $uuid = null;

    /**
     * @var string
     */
    public $reference = null;
}
