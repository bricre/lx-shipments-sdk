<?php

namespace LogisticsX\Shipments\Model\Consignment\Stock;

use OpenAPI\Runtime\AbstractModel;

class In extends AbstractModel
{
    /**
     * @var string
     */
    public $uuid = null;

    /**
     * @var string
     */
    public $reference = null;

    /**
     * @var string|null
     */
    public $ucn = null;

    /**
     * @var int
     */
    public $numberOfPallets = null;

    /**
     * @var int
     */
    public $actualNumberOfCartons = null;

    /**
     * @var string
     */
    public $termsOfSales = 'DDP';

    /**
     * @var int
     */
    public $isForPva = null;

    /**
     * @var int|null
     */
    public $clientId = null;

    /**
     * @var string|null
     */
    public $clientCode = null;

    /**
     * @var string|null
     */
    public $clientReference = null;

    /**
     * @var string|null
     */
    public $additionalReference = null;

    /**
     * @var string|null
     */
    public $exporterName = null;

    /**
     * @var string|null
     */
    public $exporterAddress = null;

    /**
     * @var string|null
     */
    public $importerVat = null;

    /**
     * @var string|null
     */
    public $importerName = null;

    /**
     * @var string|null
     */
    public $importerAddress = null;

    /**
     * @var string|null
     */
    public $customsDeclarationCurrencyCode = null;

    /**
     * @var string[]
     */
    public $productSpecialAttributes = null;

    /**
     * @var string
     */
    public $contact = null;

    /**
     * @var string|null
     */
    public $businessName = null;

    /**
     * @var string
     */
    public $addressLine1 = null;

    /**
     * @var string|null
     */
    public $addressLine2 = null;

    /**
     * @var string|null
     */
    public $addressLine3 = null;

    /**
     * @var string|null
     */
    public $city = null;

    /**
     * @var string|null
     */
    public $county = null;

    /**
     * @var string
     */
    public $countryIso = 'GB';

    /**
     * @var string|null
     */
    public $postcode = null;

    /**
     * @var string|null
     */
    public $telephone = null;

    /**
     * @var string|null
     */
    public $email = null;

    /**
     * @var float|null
     */
    public $totalFee = null;

    /**
     * @var string|null
     */
    public $purchaseOrderReference = null;

    /**
     * @var string
     */
    public $createTime = null;

    /**
     * @var string
     */
    public $updateTime = null;

    /**
     * @var string
     */
    public $status = 'PENDING';

    public $billOfLading = null;

    public $container = null;

    /**
     * @var string[]|
     */
    public $consignmentCartons = null;

    /**
     * @var string|null
     */
    public $consignmentDelivery = null;

    public $deliveryService = null;

    public $transitPallet = null;

    /**
     * @var string
     */
    public $fullAddress = null;
}
