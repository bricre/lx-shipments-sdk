<?php

namespace LogisticsX\Shipments\Model\Consignment;

use OpenAPI\Runtime\AbstractModel;

class ContainerOutput extends AbstractModel
{
    /**
     * @var string
     */
    public $uuid = null;

    /**
     * @var string
     */
    public $reference = null;

    /**
     * @var string|null
     */
    public $ucn = null;

    /**
     * @var int
     */
    public $numberOfPallets = null;

    /**
     * @var int
     */
    public $actualNumberOfCartons = null;

    /**
     * @var int
     */
    public $isForPva = null;

    /**
     * @var int|null
     */
    public $clientId = null;

    /**
     * @var string|null
     */
    public $clientCode = null;

    /**
     * @var string|null
     */
    public $additionalReference = null;

    /**
     * @var string|null
     */
    public $importerVat = null;

    /**
     * @var string
     */
    public $contact = null;

    /**
     * @var string|null
     */
    public $businessName = null;

    /**
     * @var string
     */
    public $addressLine1 = null;

    /**
     * @var string|null
     */
    public $addressLine2 = null;

    /**
     * @var string|null
     */
    public $addressLine3 = null;

    /**
     * @var string|null
     */
    public $city = null;

    /**
     * @var string|null
     */
    public $county = null;

    /**
     * @var string
     */
    public $countryIso = 'GB';

    /**
     * @var string|null
     */
    public $postcode = null;

    /**
     * @var float|null
     */
    public $totalFee = null;

    /**
     * @var string|null
     */
    public $purchaseOrderReference = null;

    /**
     * @var string
     */
    public $createTime = null;

    /**
     * @var string
     */
    public $updateTime = null;

    /**
     * @var string
     */
    public $status = 'PENDING';

    public $billOfLading = null;

    /**
     * @var \LogisticsX\Shipments\Model\ConsignmentCarton\ContainerOutput[]
     */
    public $consignmentCartons = null;

    public $consignmentDelivery = null;

    public $deliveryService = null;

    /**
     * @var float
     */
    public $totalVolume = null;

    /**
     * @var float
     */
    public $totalWeight = null;

    /**
     * @var \LogisticsX\Shipments\Model\ConsignmentCartonProductSummary\ContainerOutput
     */
    public $consignmentCartonProductSummary = null;

    /**
     * @var string
     */
    public $fullAddress = null;
}
