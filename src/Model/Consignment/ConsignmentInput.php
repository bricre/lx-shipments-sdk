<?php

namespace LogisticsX\Shipments\Model\Consignment;

use OpenAPI\Runtime\AbstractModel;

class ConsignmentInput extends AbstractModel
{
    /**
     * @var string
     */
    public $reference = null;

    /**
     * @var string|null
     */
    public $ucn = null;

    /**
     * @var int
     */
    public $numberOfPallets = null;

    /**
     * @var int
     */
    public $actualNumberOfCartons = null;

    /**
     * @var string
     */
    public $termsOfSales = 'DDP';

    /**
     * @var int
     */
    public $isForPva = null;

    /**
     * @var int|null
     */
    public $clientId = null;

    /**
     * @var string|null
     */
    public $clientCode = null;

    /**
     * @var string|null
     */
    public $clientReference = null;

    /**
     * @var string|null
     */
    public $additionalReference = null;

    /**
     * @var string|null
     */
    public $exporterName = null;

    /**
     * @var string|null
     */
    public $exporterAddress = null;

    /**
     * @var string|null
     */
    public $importerVat = null;

    /**
     * @var string|null
     */
    public $importerName = null;

    /**
     * @var string|null
     */
    public $importerAddress = null;

    /**
     * @var string|null
     */
    public $customsDeclarationCurrencyCode = null;

    /**
     * @var string[]
     */
    public $productSpecialAttributes = null;

    /**
     * @var string
     */
    public $contact = null;

    /**
     * @var string|null
     */
    public $businessName = null;

    /**
     * @var string
     */
    public $addressLine1 = null;

    /**
     * @var string|null
     */
    public $addressLine2 = null;

    /**
     * @var string|null
     */
    public $addressLine3 = null;

    /**
     * @var string|null
     */
    public $city = null;

    /**
     * @var string|null
     */
    public $county = null;

    /**
     * @var string
     */
    public $countryIso = 'GB';

    /**
     * @var string|null
     */
    public $postcode = null;

    /**
     * @var string|null
     */
    public $telephone = null;

    /**
     * @var string|null
     */
    public $email = null;

    /**
     * @var float|null
     */
    public $totalFee = null;

    /**
     * @var string|null
     */
    public $purchaseOrderReference = null;

    /**
     * @var string
     */
    public $status = 'PENDING';

    /**
     * @var string|null
     */
    public $billOfLading = null;

    /**
     * @var string|null
     */
    public $masterWaybill = null;

    /**
     * @var string|null
     */
    public $container = null;

    /**
     * @var \LogisticsX\Shipments\Model\ConsignmentCarton\ConsignmentInput[]
     */
    public $consignmentCartons = null;

    public $consignmentDelivery = null;

    /**
     * @var string|null
     */
    public $deliveryService = null;

    /**
     * @var int
     */
    public $defaultNumberOfCartons = null;

    /**
     * @var string|null
     */
    public $transitPallet = null;
}
