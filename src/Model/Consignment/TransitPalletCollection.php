<?php

namespace LogisticsX\Shipments\Model\Consignment;

use OpenAPI\Runtime\AbstractModel;

class TransitPalletCollection extends AbstractModel
{
    /**
     * @var string
     */
    public $reference = null;

    /**
     * @var string
     */
    public $contact = null;

    /**
     * @var string|null
     */
    public $businessName = null;

    /**
     * @var string
     */
    public $addressLine1 = null;

    /**
     * @var string|null
     */
    public $addressLine2 = null;

    /**
     * @var string|null
     */
    public $addressLine3 = null;

    /**
     * @var string|null
     */
    public $city = null;

    /**
     * @var string|null
     */
    public $county = null;

    /**
     * @var string
     */
    public $countryIso = 'GB';

    /**
     * @var string|null
     */
    public $postcode = null;

    /**
     * @var string|null
     */
    public $telephone = null;

    /**
     * @var string|null
     */
    public $email = null;

    public $deliveryService = null;

    /**
     * @var string
     */
    public $fullAddress = null;
}
