<?php

namespace LogisticsX\Shipments\Model\BillOfLadingCollection\BillOfLadingCollection;

use OpenAPI\Runtime\AbstractModel;

class CreateContainer extends AbstractModel
{
    /**
     * @var string[]|
     */
    public $billOfLadings = null;

    /**
     * @var string|null
     */
    public $containerReference = null;

    /**
     * @var string|null
     */
    public $container = null;

    /**
     * @var string|null
     */
    public $clientCode = null;
}
