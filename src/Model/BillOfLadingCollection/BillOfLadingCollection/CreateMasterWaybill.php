<?php

namespace LogisticsX\Shipments\Model\BillOfLadingCollection\BillOfLadingCollection;

use OpenAPI\Runtime\AbstractModel;

class CreateMasterWaybill extends AbstractModel
{
    /**
     * @var string[]|
     */
    public $billOfLadings = null;

    /**
     * @var string|null
     */
    public $masterWaybillReference = null;

    /**
     * @var string|null
     */
    public $masterWaybill = null;
}
