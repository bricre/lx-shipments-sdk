<?php

namespace LogisticsX\Shipments\Model\BillOfLadingCollection;

use OpenAPI\Runtime\AbstractModel;

class BillOfLadingCollectionInput extends AbstractModel
{
    /**
     * @var \LogisticsX\Shipments\Model\BillOfLading\BillOfLadingCollectionInput[]
     */
    public $billOfLadings = null;

    /**
     * @var string|null
     */
    public $masterWaybillReference = null;

    /**
     * @var string|null
     */
    public $containerReference = null;
}
