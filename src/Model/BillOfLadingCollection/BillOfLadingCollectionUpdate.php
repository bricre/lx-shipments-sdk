<?php

namespace LogisticsX\Shipments\Model\BillOfLadingCollection;

use OpenAPI\Runtime\AbstractModel;

class BillOfLadingCollectionUpdate extends AbstractModel
{
    /**
     * @var \LogisticsX\Shipments\Model\BillOfLading\BillOfLadingCollectionUpdate[]
     */
    public $billOfLadings = null;

    /**
     * @var string|null
     */
    public $status = null;

    /**
     * @var string|null
     */
    public $customsStatus = null;
}
