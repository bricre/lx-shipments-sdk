<?php

namespace LogisticsX\Shipments\Model\ConsignmentCarton;

use OpenAPI\Runtime\AbstractModel;

class ConsignmentInput extends AbstractModel
{
    /**
     * @var string[]|
     */
    public $consignmentCartonProducts = null;
}
