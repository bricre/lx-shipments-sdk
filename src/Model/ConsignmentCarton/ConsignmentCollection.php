<?php

namespace LogisticsX\Shipments\Model\ConsignmentCarton;

use OpenAPI\Runtime\AbstractModel;

class ConsignmentCollection extends AbstractModel
{
    /**
     * @var string
     */
    public $reference = null;
}
