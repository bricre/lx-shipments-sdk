<?php

namespace LogisticsX\Shipments\Model\ConsignmentCarton\Customs;

use OpenAPI\Runtime\AbstractModel;

class Clearance extends AbstractModel
{
    /**
     * @var string
     */
    public $reference = null;

    /**
     * @var string|null
     */
    public $additionalReference = null;

    /**
     * @var string|null
     */
    public $deliveryReference = null;

    /**
     * @var string
     */
    public $length = null;

    /**
     * @var string
     */
    public $width = null;

    /**
     * @var string
     */
    public $depth = null;

    /**
     * @var string
     */
    public $weight = null;

    /**
     * @var string
     */
    public $actualLength = null;

    /**
     * @var string
     */
    public $actualWidth = null;

    /**
     * @var string
     */
    public $actualDepth = null;

    /**
     * @var string
     */
    public $actualWeight = null;

    /**
     * @var \LogisticsX\Shipments\Model\ConsignmentCartonProduct\Customs\Clearance[]
     */
    public $consignmentCartonProducts = null;
}
