<?php

namespace LogisticsX\Shipments\Model\ConsignmentCarton;

use OpenAPI\Runtime\AbstractModel;

class ConsignmentCartonProductOutput extends AbstractModel
{
    /**
     * @var string
     */
    public $uuid = null;

    /**
     * @var string
     */
    public $reference = null;
}
