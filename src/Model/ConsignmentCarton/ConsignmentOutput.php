<?php

namespace LogisticsX\Shipments\Model\ConsignmentCarton;

use OpenAPI\Runtime\AbstractModel;

class ConsignmentOutput extends AbstractModel
{
    /**
     * @var string
     */
    public $reference = null;
}
