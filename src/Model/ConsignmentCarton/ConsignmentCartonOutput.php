<?php

namespace LogisticsX\Shipments\Model\ConsignmentCarton;

use OpenAPI\Runtime\AbstractModel;

class ConsignmentCartonOutput extends AbstractModel
{
    /**
     * @var string
     */
    public $uuid = null;

    /**
     * @var string
     */
    public $reference = null;

    /**
     * @var string|null
     */
    public $additionalReference = null;

    /**
     * @var string|null
     */
    public $deliveryReference = null;

    /**
     * @var string
     */
    public $length = null;

    /**
     * @var string
     */
    public $width = null;

    /**
     * @var string
     */
    public $depth = null;

    /**
     * @var string
     */
    public $weight = null;

    /**
     * @var string
     */
    public $actualLength = null;

    /**
     * @var string
     */
    public $actualWidth = null;

    /**
     * @var string
     */
    public $actualDepth = null;

    /**
     * @var string
     */
    public $actualWeight = null;

    public $consignment = null;

    /**
     * @var string|null
     */
    public $consignmentDelivery = null;
}
