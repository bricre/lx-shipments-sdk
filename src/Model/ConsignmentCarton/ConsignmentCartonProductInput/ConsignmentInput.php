<?php

namespace LogisticsX\Shipments\Model\ConsignmentCarton\ConsignmentCartonProductInput;

use OpenAPI\Runtime\AbstractModel;

class ConsignmentInput extends AbstractModel
{
    /**
     * @var
     * \LogisticsX\Shipments\Model\ConsignmentCartonProduct\ConsignmentCartonProductInput\ConsignmentInput[]
     */
    public $consignmentCartonProducts = null;
}
