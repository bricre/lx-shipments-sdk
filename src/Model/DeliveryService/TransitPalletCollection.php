<?php

namespace LogisticsX\Shipments\Model\DeliveryService;

use OpenAPI\Runtime\AbstractModel;

class TransitPalletCollection extends AbstractModel
{
    /**
     * @var string|null
     */
    public $name = null;
}
