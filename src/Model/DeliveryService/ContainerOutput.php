<?php

namespace LogisticsX\Shipments\Model\DeliveryService;

use OpenAPI\Runtime\AbstractModel;

class ContainerOutput extends AbstractModel
{
    /**
     * @var string
     */
    public $code = null;

    /**
     * @var string|null
     */
    public $name = null;

    /**
     * @var string|null
     */
    public $deliveryServiceTypeCode = null;
}
