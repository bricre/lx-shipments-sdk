<?php

namespace LogisticsX\Shipments\Model\DeliveryService\Stock;

use OpenAPI\Runtime\AbstractModel;

class In extends AbstractModel
{
    /**
     * @var string
     */
    public $code = null;

    /**
     * @var string|null
     */
    public $name = null;
}
