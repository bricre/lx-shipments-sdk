<?php

namespace LogisticsX\Shipments\Model\DeliveryService;

use OpenAPI\Runtime\AbstractModel;

class ConsignmentOutput extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string
     */
    public $code = null;

    /**
     * @var string|null
     */
    public $name = null;

    /**
     * @var string|null
     */
    public $deliveryServiceTypeCode = null;
}
