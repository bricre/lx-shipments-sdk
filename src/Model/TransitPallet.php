<?php

namespace LogisticsX\Shipments\Model;

use OpenAPI\Runtime\AbstractModel;

class TransitPallet extends AbstractModel
{
    /**
     * @var string
     */
    public $id = null;

    /**
     * @var string|null
     */
    public $reference = null;

    /**
     * @var int
     */
    public $quantity = 1;

    /**
     * @var string
     */
    public $location = 'Default';

    /**
     * @var int
     */
    public $size = null;

    /**
     * @var string[]|
     */
    public $consignments = null;

    /**
     * @var string|null
     */
    public $outStockOrder = null;
}
