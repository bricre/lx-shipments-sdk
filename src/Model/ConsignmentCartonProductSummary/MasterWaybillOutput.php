<?php

namespace LogisticsX\Shipments\Model\ConsignmentCartonProductSummary;

use OpenAPI\Runtime\AbstractModel;

class MasterWaybillOutput extends AbstractModel
{
    /**
     * @var string[]
     */
    public $names = null;

    /**
     * @var string[]
     */
    public $namesChinese = null;

    /**
     * @var string[]
     */
    public $commodityCodes = null;

    /**
     * @var string[]
     */
    public $materials = null;

    /**
     * @var float
     */
    public $totalPrice = null;

    /**
     * @var int
     */
    public $totalQuantity = null;

    /**
     * @var int
     */
    public $totalCartonQuantity = null;

    /**
     * @var float
     */
    public $totalCartonVolume = null;

    /**
     * @var float
     */
    public $totalCartonWeight = null;

    /**
     * @var float
     */
    public $totalCartonActualWeight = null;
}
