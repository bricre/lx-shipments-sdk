<?php

namespace LogisticsX\Shipments\Model\ProductSpecialAttribute;

use OpenAPI\Runtime\AbstractModel;

class ProductSpecialAttributeInput extends AbstractModel
{
    /**
     * @var string
     */
    public $code = null;

    /**
     * @var string
     */
    public $name = null;
}
