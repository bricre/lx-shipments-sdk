<?php

namespace LogisticsX\Shipments\Model\ProductSpecialAttribute;

use OpenAPI\Runtime\AbstractModel;

class ProductSpecialAttributeOutput extends AbstractModel
{
    /**
     * @var string
     */
    public $code = null;

    /**
     * @var string
     */
    public $name = null;
}
