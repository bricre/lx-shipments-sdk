<?php

namespace LogisticsX\Shipments\Model\BillOfLadingDelivery;

use OpenAPI\Runtime\AbstractModel;

class BillOfLadingOutput extends AbstractModel
{
    /**
     * @var string|null
     */
    public $deliveryReference = null;

    public $deliveryService = null;

    /**
     * @var string
     */
    public $fullAddress = null;
}
