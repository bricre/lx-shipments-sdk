<?php

namespace LogisticsX\Shipments\Model\ConsignmentCartonCollection;

use OpenAPI\Runtime\AbstractModel;

class ConsignmentCartonCollectionInput extends AbstractModel
{
    /**
     * @var string[]|
     */
    public $consignmentCartons = null;
}
