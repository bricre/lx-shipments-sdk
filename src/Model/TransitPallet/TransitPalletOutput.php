<?php

namespace LogisticsX\Shipments\Model\TransitPallet;

use OpenAPI\Runtime\AbstractModel;

class TransitPalletOutput extends AbstractModel
{
    /**
     * @var string[]|
     */
    public $consignments = null;
}
