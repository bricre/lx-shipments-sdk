<?php

namespace LogisticsX\Shipments\Model\TransitPallet;

use OpenAPI\Runtime\AbstractModel;

class TransitPalletCollection extends AbstractModel
{
    /**
     * @var string|null
     */
    public $reference = null;

    /**
     * @var int
     */
    public $quantity = 1;

    /**
     * @var string
     */
    public $location = 'Default';

    /**
     * @var int
     */
    public $size = null;

    /**
     * @var \LogisticsX\Shipments\Model\Consignment\TransitPalletCollection[]
     */
    public $consignments = null;

    public $outStockOrder = null;
}
