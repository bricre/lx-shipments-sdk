<?php

namespace LogisticsX\Shipments\Model\TransitPallet\Stock;

use OpenAPI\Runtime\AbstractModel;

class In extends AbstractModel
{
    /**
     * @var string|null
     */
    public $reference = null;

    /**
     * @var int
     */
    public $quantity = 1;

    /**
     * @var string
     */
    public $location = 'Default';
}
