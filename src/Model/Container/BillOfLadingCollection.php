<?php

namespace LogisticsX\Shipments\Model\Container;

use OpenAPI\Runtime\AbstractModel;

class BillOfLadingCollection extends AbstractModel
{
    /**
     * @var string
     */
    public $reference = null;
}
