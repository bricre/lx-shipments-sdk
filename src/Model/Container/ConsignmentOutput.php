<?php

namespace LogisticsX\Shipments\Model\Container;

use OpenAPI\Runtime\AbstractModel;

class ConsignmentOutput extends AbstractModel
{
    /**
     * @var string
     */
    public $uuid = null;

    /**
     * @var string
     */
    public $reference = null;

    /**
     * @var string|null
     */
    public $meansOfTransportCode = null;

    /**
     * @var string|null
     */
    public $size = null;

    /**
     * @var int
     */
    public $demurrageNumberOfDay = null;

    /**
     * @var int
     */
    public $detentionNumberOfDay = null;
}
