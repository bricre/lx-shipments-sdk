<?php

namespace LogisticsX\Shipments\Model\Container;

use OpenAPI\Runtime\AbstractModel;

class ContainerInput extends AbstractModel
{
    /**
     * @var int|null
     */
    public $clientId = null;

    /**
     * @var string|null
     */
    public $clientCode = null;

    /**
     * @var string
     */
    public $reference = null;

    /**
     * @var string|null
     */
    public $meansOfTransportCode = null;

    /**
     * @var string|null
     */
    public $size = null;

    /**
     * @var string|null
     */
    public $releaseCode = null;

    /**
     * @var string|null
     */
    public $releaseCodeExpiryTime = null;

    /**
     * @var int
     */
    public $demurrageNumberOfDay = null;

    /**
     * @var int
     */
    public $detentionNumberOfDay = null;

    /**
     * @var string
     */
    public $deliveryType = 'BONDED_WAREHOUSE';

    /**
     * @var string|null
     */
    public $deliveryConsignee = null;

    /**
     * @var string|null
     */
    public $deliveryAddress = null;

    /**
     * @var string|null
     */
    public $ucnPrefix = null;

    /**
     * @var string|null
     */
    public $scheduledCollectionDate = null;

    /**
     * @var string|null
     */
    public $status = null;

    /**
     * @var string|null
     */
    public $masterWaybill = null;

    /**
     * @var string[]|
     */
    public $consignments = null;

    /**
     * @var string[]|
     */
    public $billOfLadings = null;
}
