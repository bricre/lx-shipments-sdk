<?php

namespace LogisticsX\Shipments\Model\Container;

use OpenAPI\Runtime\AbstractModel;

class BillOfLadingOutput extends AbstractModel
{
    /**
     * @var string
     */
    public $reference = null;

    /**
     * @var string|null
     */
    public $meansOfTransportCode = null;

    /**
     * @var string|null
     */
    public $size = null;

    /**
     * @var string
     */
    public $createTime = null;

    /**
     * @var string|null
     */
    public $updateTime = null;

    /**
     * @var string|null
     */
    public $status = null;

    /**
     * @var
     * \LogisticsX\Shipments\Model\ConsignmentCartonProductSummary\BillOfLadingOutput
     */
    public $consignmentCartonProductSummary = null;
}
