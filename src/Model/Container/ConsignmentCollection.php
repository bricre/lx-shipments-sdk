<?php

namespace LogisticsX\Shipments\Model\Container;

use OpenAPI\Runtime\AbstractModel;

class ConsignmentCollection extends AbstractModel
{
    /**
     * @var string
     */
    public $uuid = null;

    /**
     * @var string
     */
    public $reference = null;

    /**
     * @var string|null
     */
    public $size = null;

    /**
     * @var string|null
     */
    public $releaseCode = null;

    /**
     * @var string|null
     */
    public $releaseCodeExpiryTime = null;

    /**
     * @var int
     */
    public $demurrageNumberOfDay = null;

    /**
     * @var int
     */
    public $detentionNumberOfDay = null;
}
