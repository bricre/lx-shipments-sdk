<?php

namespace LogisticsX\Shipments\Model\Container\Customs;

use OpenAPI\Runtime\AbstractModel;

class Clearance extends AbstractModel
{
    /**
     * @var string
     */
    public $reference = null;

    /**
     * @var string|null
     */
    public $scheduledCollectionDate = null;
}
