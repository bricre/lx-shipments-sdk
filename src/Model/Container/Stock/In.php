<?php

namespace LogisticsX\Shipments\Model\Container\Stock;

use OpenAPI\Runtime\AbstractModel;

class In extends AbstractModel
{
    /**
     * @var string
     */
    public $reference = null;

    /**
     * @var string|null
     */
    public $scheduledCollectionDate = null;
}
