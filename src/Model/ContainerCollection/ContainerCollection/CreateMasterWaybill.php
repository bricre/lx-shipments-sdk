<?php

namespace LogisticsX\Shipments\Model\ContainerCollection\ContainerCollection;

use OpenAPI\Runtime\AbstractModel;

class CreateMasterWaybill extends AbstractModel
{
    /**
     * @var string[]|
     */
    public $containers = null;

    /**
     * @var string|null
     */
    public $masterWaybillReference = null;

    /**
     * @var string|null
     */
    public $masterWaybill = null;

    /**
     * @var string|null
     */
    public $clientCode = null;
}
