<?php

namespace LogisticsX\Shipments\Model\ContainerCollection;

use OpenAPI\Runtime\AbstractModel;

class ContainerCollectionUpdate extends AbstractModel
{
    /**
     * @var string[]|
     */
    public $containers = null;

    /**
     * @var string|null
     */
    public $status = null;
}
