<?php

namespace LogisticsX\Shipments\Model\CustomsBroker;

use OpenAPI\Runtime\AbstractModel;

/**
 * customsBroker.
 */
class CustomsBrokerInput extends AbstractModel
{
    /**
     * @var string
     */
    public $name = null;

    /**
     * @var string
     */
    public $contact = null;

    /**
     * @var string|null
     */
    public $businessName = null;

    /**
     * @var string
     */
    public $addressLine1 = null;

    /**
     * @var string|null
     */
    public $addressLine2 = null;

    /**
     * @var string|null
     */
    public $addressLine3 = null;

    /**
     * @var string|null
     */
    public $city = null;

    /**
     * @var string|null
     */
    public $county = null;

    /**
     * @var string
     */
    public $countryIso = null;

    /**
     * @var string|null
     */
    public $postcode = null;

    /**
     * @var string|null
     */
    public $telephone = null;

    /**
     * @var string|null
     */
    public $email = null;
}
