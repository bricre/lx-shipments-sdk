<?php

namespace LogisticsX\Shipments\Model\CustomsBroker;

use OpenAPI\Runtime\AbstractModel;

/**
 * customsBroker.
 */
class BillOfLadingOutput extends AbstractModel
{
    /**
     * @var string
     */
    public $name = null;
}
