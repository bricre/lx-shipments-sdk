<?php

namespace LogisticsX\Shipments\Model\ConsignmentCartonProduct\ConsignmentCartonProductInput;

use OpenAPI\Runtime\AbstractModel;

class ConsignmentInput extends AbstractModel
{
    /**
     * @var string|null
     */
    public $name = null;

    /**
     * @var string|null
     */
    public $nameChinese = null;

    /**
     * @var string
     */
    public $price = null;

    /**
     * @var int|null
     */
    public $quantity = null;

    /**
     * @var string
     */
    public $weight = null;

    /**
     * @var string
     */
    public $actualWeight = null;

    /**
     * @var string|null
     */
    public $commodityCode = null;

    /**
     * @var string|null
     */
    public $material = null;

    public $consignmentCarton = null;
}
