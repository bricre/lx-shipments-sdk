<?php

namespace LogisticsX\Shipments\Model\ConsignmentCartonProduct\ConsignmentCartonProduct;

use OpenAPI\Runtime\AbstractModel;

class SummaryByConsignment extends AbstractModel
{
    /**
     * @var string|null
     */
    public $name = null;

    /**
     * @var string|null
     */
    public $nameChinese = null;

    /**
     * @var string
     */
    public $price = null;

    /**
     * @var string
     */
    public $weight = null;

    /**
     * @var string
     */
    public $actualWeight = null;

    /**
     * @var string|null
     */
    public $commodityCode = null;

    /**
     * @var string|null
     */
    public $material = null;

    /**
     * @var int
     */
    public $quantitySummary = null;
}
