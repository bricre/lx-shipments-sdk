<?php

namespace LogisticsX\Shipments\Model\ConsignmentCartonProduct;

use OpenAPI\Runtime\AbstractModel;

class ContainerOutput extends AbstractModel
{
    /**
     * @var string|null
     */
    public $name = null;

    /**
     * @var string|null
     */
    public $nameChinese = null;

    /**
     * @var string
     */
    public $price = null;

    /**
     * @var int|null
     */
    public $quantity = null;

    /**
     * @var string
     */
    public $weight = null;

    /**
     * @var string
     */
    public $actualWeight = null;

    /**
     * @var string|null
     */
    public $commodityCode = null;

    /**
     * @var string|null
     */
    public $material = null;
}
