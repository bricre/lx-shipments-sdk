<?php

namespace LogisticsX\Shipments\Model\ConsignmentCollection;

use OpenAPI\Runtime\AbstractModel;

class ConsignmentCollectionUpdate extends AbstractModel
{
    /**
     * @var string[]|
     */
    public $consignments = null;

    /**
     * @var string|null
     */
    public $status = null;
}
