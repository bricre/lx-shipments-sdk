<?php

namespace LogisticsX\Shipments\Model\ConsignmentCollection;

use OpenAPI\Runtime\AbstractModel;

class ConsignmentCollectionInput extends AbstractModel
{
    /**
     * @var string[]|
     */
    public $consignments = null;

    /**
     * @var string|null
     */
    public $billOfLadingReference = null;

    /**
     * @var string|null
     */
    public $clientCode = null;

    /**
     * @var string|null
     */
    public $exporterAddress = null;

    /**
     * @var string|null
     */
    public $exporterName = null;

    /**
     * @var string|null
     */
    public $importerName = null;

    /**
     * @var string|null
     */
    public $importerAddress = null;

    /**
     * @var string|null
     */
    public $importerVat = null;

    /**
     * @var bool
     */
    public $isForPva = null;

    /**
     * @var string|null
     */
    public $termsOfSales = null;

    /**
     * @var string|null
     */
    public $generateType = null;
}
