<?php

namespace LogisticsX\Shipments\Model\ConsignmentCollection\ConsignmentCollection;

use OpenAPI\Runtime\AbstractModel;

class CreateBillOfLading extends AbstractModel
{
    /**
     * @var string[]|
     */
    public $consignments = null;

    /**
     * @var string|null
     */
    public $billOfLading = null;
}
