<?php

namespace LogisticsX\Shipments\Model\ConsignmentCollection;

use OpenAPI\Runtime\AbstractModel;

class ConsignmentCollectionOutput extends AbstractModel
{
    /**
     * @var string[]|
     */
    public $consignments = null;
}
