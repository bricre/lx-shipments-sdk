<?php

namespace LogisticsX\Shipments\Api;

use LogisticsX\Shipments\Model\ConsignmentFee\ConsignmentFeeInput;
use LogisticsX\Shipments\Model\ConsignmentFee\ConsignmentFeeOutput;

class ConsignmentFee extends AbstractAPI
{
    /**
     * Retrieves the collection of ConsignmentFee resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'id'	integer
     *                       'id[]'	array
     *                       'consignment.uuid'	string
     *                       'consignment.reference'	string
     *                       'amount'	string
     *                       'amount[]'	array
     *                       'order[id]'	string
     *                       'order[amount]'	string
     *                       'order[consignment.uuid]'	string
     *                       'order[consignment.reference]'	string
     *
     * @return ConsignmentFeeOutput[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getConsignmentFeeCollection',
        'GET',
        'api/shipments/consignment_fees',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a ConsignmentFee resource.
     *
     * @param ConsignmentFeeInput $Model The new ConsignmentFee resource
     *
     * @return ConsignmentFeeOutput
     */
    public function postCollection(ConsignmentFeeInput $Model): ConsignmentFeeOutput
    {
        return $this->request(
        'postConsignmentFeeCollection',
        'POST',
        'api/shipments/consignment_fees',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a ConsignmentFee resource.
     *
     * @param string $id Resource identifier
     *
     * @return ConsignmentFeeOutput|null
     */
    public function getItem(string $id): ?ConsignmentFeeOutput
    {
        return $this->request(
        'getConsignmentFeeItem',
        'GET',
        "api/shipments/consignment_fees/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the ConsignmentFee resource.
     *
     * @param string              $id    Resource identifier
     * @param ConsignmentFeeInput $Model The updated ConsignmentFee resource
     *
     * @return ConsignmentFeeOutput
     */
    public function putItem(string $id, ConsignmentFeeInput $Model): ConsignmentFeeOutput
    {
        return $this->request(
        'putConsignmentFeeItem',
        'PUT',
        "api/shipments/consignment_fees/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the ConsignmentFee resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteConsignmentFeeItem',
        'DELETE',
        "api/shipments/consignment_fees/$id",
        null,
        [],
        []
        );
    }
}
