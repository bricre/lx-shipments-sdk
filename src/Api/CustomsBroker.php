<?php

namespace LogisticsX\Shipments\Api;

use LogisticsX\Shipments\Model\CustomsBroker\CustomsBrokerInput;

class CustomsBroker extends AbstractAPI
{
    /**
     * Retrieves the collection of CustomsBroker resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'id'	integer
     *                       'id[]'	array
     *                       'uuid'	string
     *                       'name'	string
     *                       'order[id]'	string
     *                       'order[uuid]'	string
     *                       'order[name]'	string
     *
     * @return CustomsBrokerInput[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getCustomsBrokerCollection',
        'GET',
        'api/shipments/customs_brokers',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a CustomsBroker resource.
     *
     * @param CustomsBrokerInput $Model The new CustomsBroker resource
     *
     * @return CustomsBrokerInput
     */
    public function postCollection(CustomsBrokerInput $Model): CustomsBrokerInput
    {
        return $this->request(
        'postCustomsBrokerCollection',
        'POST',
        'api/shipments/customs_brokers',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a CustomsBroker resource.
     *
     * @param string $uuid Resource identifier
     *
     * @return CustomsBrokerInput|null
     */
    public function getItem(string $uuid): ?CustomsBrokerInput
    {
        return $this->request(
        'getCustomsBrokerItem',
        'GET',
        "api/shipments/customs_brokers/$uuid",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the CustomsBroker resource.
     *
     * @param string             $uuid  Resource identifier
     * @param CustomsBrokerInput $Model The updated CustomsBroker resource
     *
     * @return CustomsBrokerInput
     */
    public function putItem(string $uuid, CustomsBrokerInput $Model): CustomsBrokerInput
    {
        return $this->request(
        'putCustomsBrokerItem',
        'PUT',
        "api/shipments/customs_brokers/$uuid",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the CustomsBroker resource.
     *
     * @param string $uuid Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $uuid): mixed
    {
        return $this->request(
        'deleteCustomsBrokerItem',
        'DELETE',
        "api/shipments/customs_brokers/$uuid",
        null,
        [],
        []
        );
    }
}
