<?php

namespace LogisticsX\Shipments\Api;

use LogisticsX\Shipments\Model\ConsignmentCartonCollection\ConsignmentCartonCollectionOutput;

class ConsignmentCartonCollection extends AbstractAPI
{
    /**
     * Creates a ConsignmentCartonCollection resource.
     *
     * @return ConsignmentCartonCollectionOutput
     */
    public function postCollection(): ConsignmentCartonCollectionOutput
    {
        return $this->request(
        'postConsignmentCartonCollectionCollection',
        'POST',
        'api/shipments/consignment_carton_collections',
        null,
        [],
        []
        );
    }

    /**
     * Retrieves a ConsignmentCartonCollection resource.
     *
     * @param string $id Resource identifier
     *
     * @return ConsignmentCartonCollectionOutput|null
     */
    public function getItem(string $id): ?ConsignmentCartonCollectionOutput
    {
        return $this->request(
        'getConsignmentCartonCollectionItem',
        'GET',
        "api/shipments/consignment_carton_collections/$id",
        null,
        [],
        []
        );
    }
}
