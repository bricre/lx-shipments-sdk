<?php

namespace LogisticsX\Shipments\Api;

use LogisticsX\Shipments\Model\DeliveryService\DeliveryServiceRead;

class DeliveryService extends AbstractAPI
{
    /**
     * Retrieves the collection of DeliveryService resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'code'	string
     *                       'name'	string
     *                       'id'	integer
     *                       'id[]'	array
     *
     * @return DeliveryServiceRead[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getDeliveryServiceCollection',
        'GET',
        'api/shipments/delivery_services',
        null,
        $queries,
        []
        );
    }

    /**
     * Retrieves a DeliveryService resource.
     *
     * @param string $id Resource identifier
     *
     * @return DeliveryServiceRead|null
     */
    public function getItem(string $id): ?DeliveryServiceRead
    {
        return $this->request(
        'getDeliveryServiceItem',
        'GET',
        "api/shipments/delivery_services/$id",
        null,
        [],
        []
        );
    }
}
