<?php

namespace LogisticsX\Shipments\Api;

use LogisticsX\Shipments\Model\MasterWaybill\MasterWaybillInput;
use LogisticsX\Shipments\Model\MasterWaybill\MasterWaybillOutput;
use LogisticsX\Shipments\Model\MasterWaybill\Statistics;

class MasterWaybill extends AbstractAPI
{
    /**
     * Retrieves the collection of MasterWaybill resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'uuid'	string
     *                       'uuid[]'	string
     *                       'reference'	string
     *                       'clientCode'	string
     *                       'meansOfTransportCode'	string
     *                       'placeOfDepartureLocationCode'	string
     *                       'placeOfDepartureLocationName'	string
     *                       'placeOfDeliveryLocationCode'	string
     *                       'placeOfDeliveryLocationName'	string
     *                       'logisticsProviderId'	integer
     *                       'logisticsProviderId[]'	array
     *                       'logisticsProviderCode'	string
     *                       'status'	string
     *                       'status[]'	array
     *                       'order[id]'	string
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'updateTime[before]'	string
     *                       'updateTime[strictly_before]'	string
     *                       'updateTime[after]'	string
     *                       'updateTime[strictly_after]'	string
     *                       'estimatedDateOfDeparture[before]'	string
     *                       'estimatedDateOfDeparture[strictly_before]'	string
     *                       'estimatedDateOfDeparture[after]'	string
     *                       'estimatedDateOfDeparture[strictly_after]'	string
     *                       'estimatedDateOfArrival[before]'	string
     *                       'estimatedDateOfArrival[strictly_before]'	string
     *                       'estimatedDateOfArrival[after]'	string
     *                       'estimatedDateOfArrival[strictly_after]'	string
     *                       'actualDateOfDeparture[before]'	string
     *                       'actualDateOfDeparture[strictly_before]'	string
     *                       'actualDateOfDeparture[after]'	string
     *                       'actualDateOfDeparture[strictly_after]'	string
     *                       'actualDateOfArrival[before]'	string
     *                       'actualDateOfArrival[strictly_before]'	string
     *                       'actualDateOfArrival[after]'	string
     *                       'actualDateOfArrival[strictly_after]'	string
     *
     * @return MasterWaybillOutput[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getMasterWaybillCollection',
        'GET',
        'api/shipments/master_waybills',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a MasterWaybill resource.
     *
     * @param MasterWaybillInput $Model The new MasterWaybill resource
     *
     * @return MasterWaybillOutput
     */
    public function postCollection(MasterWaybillInput $Model): MasterWaybillOutput
    {
        return $this->request(
        'postMasterWaybillCollection',
        'POST',
        'api/shipments/master_waybills',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves the collection of MasterWaybill resources.
     *
     * @param string $billOfLadingUuid
     * @param array  $queries          options:
     *                                 'page'	integer	The collection page number
     *                                 'itemsPerPage'	integer	The number of items per page
     *                                 'uuid'	string
     *                                 'uuid[]'	string
     *                                 'reference'	string
     *                                 'clientCode'	string
     *                                 'meansOfTransportCode'	string
     *                                 'placeOfDepartureLocationCode'	string
     *                                 'placeOfDepartureLocationName'	string
     *                                 'placeOfDeliveryLocationCode'	string
     *                                 'placeOfDeliveryLocationName'	string
     *                                 'logisticsProviderId'	integer
     *                                 'logisticsProviderId[]'	array
     *                                 'logisticsProviderCode'	string
     *                                 'status'	string
     *                                 'status[]'	array
     *                                 'order[id]'	string
     *                                 'createTime[before]'	string
     *                                 'createTime[strictly_before]'	string
     *                                 'createTime[after]'	string
     *                                 'createTime[strictly_after]'	string
     *                                 'updateTime[before]'	string
     *                                 'updateTime[strictly_before]'	string
     *                                 'updateTime[after]'	string
     *                                 'updateTime[strictly_after]'	string
     *                                 'estimatedDateOfDeparture[before]'	string
     *                                 'estimatedDateOfDeparture[strictly_before]'	string
     *                                 'estimatedDateOfDeparture[after]'	string
     *                                 'estimatedDateOfDeparture[strictly_after]'	string
     *                                 'estimatedDateOfArrival[before]'	string
     *                                 'estimatedDateOfArrival[strictly_before]'	string
     *                                 'estimatedDateOfArrival[after]'	string
     *                                 'estimatedDateOfArrival[strictly_after]'	string
     *                                 'actualDateOfDeparture[before]'	string
     *                                 'actualDateOfDeparture[strictly_before]'	string
     *                                 'actualDateOfDeparture[after]'	string
     *                                 'actualDateOfDeparture[strictly_after]'	string
     *                                 'actualDateOfArrival[before]'	string
     *                                 'actualDateOfArrival[strictly_before]'	string
     *                                 'actualDateOfArrival[after]'	string
     *                                 'actualDateOfArrival[strictly_after]'	string
     *
     * @return MasterWaybillOutput[]|null
     */
    public function getLinkableByBillOfLadingCollection(string $billOfLadingUuid, array $queries = []): ?array
    {
        return $this->request(
        'getLinkableByBillOfLadingMasterWaybillCollection',
        'GET',
        "api/shipments/master_waybills/get_linkable_by_bill_of_lading/$billOfLadingUuid",
        null,
        $queries,
        []
        );
    }

    /**
     * Retrieves the collection of MasterWaybill resources.
     *
     * @param string $consignmentUuid
     * @param array  $queries         options:
     *                                'page'	integer	The collection page number
     *                                'itemsPerPage'	integer	The number of items per page
     *                                'uuid'	string
     *                                'uuid[]'	string
     *                                'reference'	string
     *                                'clientCode'	string
     *                                'meansOfTransportCode'	string
     *                                'placeOfDepartureLocationCode'	string
     *                                'placeOfDepartureLocationName'	string
     *                                'placeOfDeliveryLocationCode'	string
     *                                'placeOfDeliveryLocationName'	string
     *                                'logisticsProviderId'	integer
     *                                'logisticsProviderId[]'	array
     *                                'logisticsProviderCode'	string
     *                                'status'	string
     *                                'status[]'	array
     *                                'order[id]'	string
     *                                'createTime[before]'	string
     *                                'createTime[strictly_before]'	string
     *                                'createTime[after]'	string
     *                                'createTime[strictly_after]'	string
     *                                'updateTime[before]'	string
     *                                'updateTime[strictly_before]'	string
     *                                'updateTime[after]'	string
     *                                'updateTime[strictly_after]'	string
     *                                'estimatedDateOfDeparture[before]'	string
     *                                'estimatedDateOfDeparture[strictly_before]'	string
     *                                'estimatedDateOfDeparture[after]'	string
     *                                'estimatedDateOfDeparture[strictly_after]'	string
     *                                'estimatedDateOfArrival[before]'	string
     *                                'estimatedDateOfArrival[strictly_before]'	string
     *                                'estimatedDateOfArrival[after]'	string
     *                                'estimatedDateOfArrival[strictly_after]'	string
     *                                'actualDateOfDeparture[before]'	string
     *                                'actualDateOfDeparture[strictly_before]'	string
     *                                'actualDateOfDeparture[after]'	string
     *                                'actualDateOfDeparture[strictly_after]'	string
     *                                'actualDateOfArrival[before]'	string
     *                                'actualDateOfArrival[strictly_before]'	string
     *                                'actualDateOfArrival[after]'	string
     *                                'actualDateOfArrival[strictly_after]'	string
     *
     * @return MasterWaybillOutput[]|null
     */
    public function getLinkableByConsignmentCollection(string $consignmentUuid, array $queries = []): ?array
    {
        return $this->request(
        'getLinkableByConsignmentMasterWaybillCollection',
        'GET',
        "api/shipments/master_waybills/get_linkable_by_consignment/$consignmentUuid",
        null,
        $queries,
        []
        );
    }

    /**
     * Retrieves the collection of MasterWaybill resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'uuid'	string
     *                       'uuid[]'	string
     *                       'reference'	string
     *                       'clientCode'	string
     *                       'meansOfTransportCode'	string
     *                       'placeOfDepartureLocationCode'	string
     *                       'placeOfDepartureLocationName'	string
     *                       'placeOfDeliveryLocationCode'	string
     *                       'placeOfDeliveryLocationName'	string
     *                       'logisticsProviderId'	integer
     *                       'logisticsProviderId[]'	array
     *                       'logisticsProviderCode'	string
     *                       'status'	string
     *                       'status[]'	array
     *                       'order[id]'	string
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'updateTime[before]'	string
     *                       'updateTime[strictly_before]'	string
     *                       'updateTime[after]'	string
     *                       'updateTime[strictly_after]'	string
     *                       'estimatedDateOfDeparture[before]'	string
     *                       'estimatedDateOfDeparture[strictly_before]'	string
     *                       'estimatedDateOfDeparture[after]'	string
     *                       'estimatedDateOfDeparture[strictly_after]'	string
     *                       'estimatedDateOfArrival[before]'	string
     *                       'estimatedDateOfArrival[strictly_before]'	string
     *                       'estimatedDateOfArrival[after]'	string
     *                       'estimatedDateOfArrival[strictly_after]'	string
     *                       'actualDateOfDeparture[before]'	string
     *                       'actualDateOfDeparture[strictly_before]'	string
     *                       'actualDateOfDeparture[after]'	string
     *                       'actualDateOfDeparture[strictly_after]'	string
     *                       'actualDateOfArrival[before]'	string
     *                       'actualDateOfArrival[strictly_before]'	string
     *                       'actualDateOfArrival[after]'	string
     *                       'actualDateOfArrival[strictly_after]'	string
     *
     * @return Statistics[]|null
     */
    public function statusStatisticsCollection(array $queries = []): ?array
    {
        return $this->request(
        'statusStatisticsMasterWaybillCollection',
        'GET',
        'api/shipments/master_waybills/statistics',
        null,
        $queries,
        []
        );
    }

    /**
     * Retrieves a MasterWaybill resource.
     *
     * @param string $uuid Resource identifier
     *
     * @return MasterWaybillOutput|null
     */
    public function getItem(string $uuid): ?MasterWaybillOutput
    {
        return $this->request(
        'getMasterWaybillItem',
        'GET',
        "api/shipments/master_waybills/$uuid",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the MasterWaybill resource.
     *
     * @param string             $uuid  Resource identifier
     * @param MasterWaybillInput $Model The updated MasterWaybill resource
     *
     * @return MasterWaybillOutput
     */
    public function putItem(string $uuid, MasterWaybillInput $Model): MasterWaybillOutput
    {
        return $this->request(
        'putMasterWaybillItem',
        'PUT',
        "api/shipments/master_waybills/$uuid",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the MasterWaybill resource.
     *
     * @param string $uuid Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $uuid): mixed
    {
        return $this->request(
        'deleteMasterWaybillItem',
        'DELETE',
        "api/shipments/master_waybills/$uuid",
        null,
        [],
        []
        );
    }
}
