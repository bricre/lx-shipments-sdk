<?php

namespace LogisticsX\Shipments\Api;

use LogisticsX\Shipments\Model\ConsignmentCollection\ConsignmentCollection\CreateBillOfLading;
use LogisticsX\Shipments\Model\ConsignmentCollection\ConsignmentCollectionInput;
use LogisticsX\Shipments\Model\ConsignmentCollection\ConsignmentCollectionOutput;
use LogisticsX\Shipments\Model\ConsignmentCollection\ConsignmentCollectionUpdate;

class ConsignmentCollection extends AbstractAPI
{
    /**
     * Creates a ConsignmentCollection resource.
     *
     * @param ConsignmentCollectionInput $Model The new ConsignmentCollection resource
     *
     * @return ConsignmentCollectionOutput
     */
    public function postCollection(ConsignmentCollectionInput $Model): ConsignmentCollectionOutput
    {
        return $this->request(
        'postConsignmentCollectionCollection',
        'POST',
        'api/shipments/consignment_collections',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Creates a ConsignmentCollection resource.
     *
     * @param CreateBillOfLading $Model The new ConsignmentCollection resource
     *
     * @return CreateBillOfLading
     */
    public function create_bill_of_ladingCollection(CreateBillOfLading $Model): CreateBillOfLading
    {
        return $this->request(
        'create_bill_of_ladingConsignmentCollectionCollection',
        'POST',
        'api/shipments/consignment_collections/create_bill_of_lading',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a ConsignmentCollection resource.
     *
     * @param string $id Resource identifier
     *
     * @return ConsignmentCollectionOutput|null
     */
    public function getItem(string $id): ?ConsignmentCollectionOutput
    {
        return $this->request(
        'getConsignmentCollectionItem',
        'GET',
        "api/shipments/consignment_collections/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the ConsignmentCollection resource.
     *
     * @param string                      $id    Resource identifier
     * @param ConsignmentCollectionUpdate $Model The updated ConsignmentCollection
     *                                           resource
     *
     * @return ConsignmentCollectionUpdate
     */
    public function putItem(string $id, ConsignmentCollectionUpdate $Model): ConsignmentCollectionUpdate
    {
        return $this->request(
        'putConsignmentCollectionItem',
        'PUT',
        "api/shipments/consignment_collections/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }
}
