<?php

namespace LogisticsX\Shipments\Api;

use LogisticsX\Shipments\Model\TransitPallet as TransitPalletModel;
use LogisticsX\Shipments\Model\TransitPallet\TransitPalletCollection;
use LogisticsX\Shipments\Model\TransitPallet\TransitPalletOutput;

class TransitPallet extends AbstractAPI
{
    /**
     * Retrieves the collection of TransitPallet resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'reference'	string
     *                       'location'	string
     *
     * @return TransitPalletCollection[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getTransitPalletCollection',
        'GET',
        'api/shipments/transit_pallets',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a TransitPallet resource.
     *
     * @param TransitPalletModel $Model The new TransitPallet resource
     *
     * @return TransitPalletModel
     */
    public function postCollection(TransitPalletModel $Model): TransitPalletModel
    {
        return $this->request(
        'postTransitPalletCollection',
        'POST',
        'api/shipments/transit_pallets',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a TransitPallet resource.
     *
     * @param string $id Resource identifier
     *
     * @return TransitPalletOutput|null
     */
    public function getItem(string $id): ?TransitPalletOutput
    {
        return $this->request(
        'getTransitPalletItem',
        'GET',
        "api/shipments/transit_pallets/$id",
        null,
        [],
        []
        );
    }

    /**
     * Removes the TransitPallet resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteTransitPalletItem',
        'DELETE',
        "api/shipments/transit_pallets/$id",
        null,
        [],
        []
        );
    }
}
