<?php

namespace LogisticsX\Shipments\Api;

use LogisticsX\Shipments\Model\BillOfLadingCollection\BillOfLadingCollection\CreateContainer;
use LogisticsX\Shipments\Model\BillOfLadingCollection\BillOfLadingCollection\CreateMasterWaybill;
use LogisticsX\Shipments\Model\BillOfLadingCollection\BillOfLadingCollectionInput;
use LogisticsX\Shipments\Model\BillOfLadingCollection\BillOfLadingCollectionOutput;
use LogisticsX\Shipments\Model\BillOfLadingCollection\BillOfLadingCollectionUpdate;

class BillOfLadingCollection extends AbstractAPI
{
    /**
     * Creates a BillOfLadingCollection resource.
     *
     * @param BillOfLadingCollectionInput $Model The new BillOfLadingCollection
     *                                           resource
     *
     * @return BillOfLadingCollectionOutput
     */
    public function postCollection(BillOfLadingCollectionInput $Model): BillOfLadingCollectionOutput
    {
        return $this->request(
        'postBillOfLadingCollectionCollection',
        'POST',
        'api/shipments/bill_of_lading_collections',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Creates a BillOfLadingCollection resource.
     *
     * @param CreateContainer $Model The new BillOfLadingCollection resource
     *
     * @return CreateContainer
     */
    public function create_containerCollection(CreateContainer $Model): CreateContainer
    {
        return $this->request(
        'create_containerBillOfLadingCollectionCollection',
        'POST',
        'api/shipments/bill_of_lading_collections/create_container',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Creates a BillOfLadingCollection resource.
     *
     * @param CreateMasterWaybill $Model The new BillOfLadingCollection resource
     *
     * @return CreateMasterWaybill
     */
    public function create_master_waybillCollection(CreateMasterWaybill $Model): CreateMasterWaybill
    {
        return $this->request(
        'create_master_waybillBillOfLadingCollectionCollection',
        'POST',
        'api/shipments/bill_of_lading_collections/create_master_waybill',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a BillOfLadingCollection resource.
     *
     * @param string $id Resource identifier
     *
     * @return BillOfLadingCollectionOutput|null
     */
    public function getItem(string $id): ?BillOfLadingCollectionOutput
    {
        return $this->request(
        'getBillOfLadingCollectionItem',
        'GET',
        "api/shipments/bill_of_lading_collections/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the BillOfLadingCollection resource.
     *
     * @param string                       $id    Resource identifier
     * @param BillOfLadingCollectionUpdate $Model The updated BillOfLadingCollection
     *                                            resource
     *
     * @return BillOfLadingCollectionUpdate
     */
    public function putItem(string $id, BillOfLadingCollectionUpdate $Model): BillOfLadingCollectionUpdate
    {
        return $this->request(
        'putBillOfLadingCollectionItem',
        'PUT',
        "api/shipments/bill_of_lading_collections/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }
}
