<?php

namespace LogisticsX\Shipments\Api;

use LogisticsX\Shipments\Model\BillOfLading\BillOfLadingCollection;
use LogisticsX\Shipments\Model\BillOfLading\BillOfLadingInput;
use LogisticsX\Shipments\Model\BillOfLading\BillOfLadingOutput;
use LogisticsX\Shipments\Model\BillOfLading\Statistics;

class BillOfLading extends AbstractAPI
{
    /**
     * Retrieves the collection of BillOfLading resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'uuid'	string
     *                       'uuid[]'	string
     *                       'masterWaybill.uuid'	string
     *                       'masterWaybill.uuid[]'	string
     *                       'exists[masterWaybill]'	boolean
     *                       'id'	integer
     *                       'id[]'	array
     *                       'clientId'	integer
     *                       'clientId[]'	array
     *                       'clientCode'	string
     *                       'clientCode[]'	array
     *                       'reference'	string
     *                       'ucn'	string
     *                       'customsEntry'	string
     *                       'customsStatus'	string
     *                       'customsStatus[]'	array
     *                       'status'	string
     *                       'status[]'	array
     *                       'billOfLadingDelivery.deliveryService.deliveryServiceTypeCode'	string
     *                       'masterWaybill.reference'	string
     *                       'container.reference'	string
     *                       'order[id]'	string
     *                       'order[uuid]'	string
     *                       'order[clientId]'	string
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'updateTime[before]'	string
     *                       'updateTime[strictly_before]'	string
     *                       'updateTime[after]'	string
     *                       'updateTime[strictly_after]'	string
     *
     * @return BillOfLadingCollection[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getBillOfLadingCollection',
        'GET',
        'api/shipments/bill_of_ladings',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a BillOfLading resource.
     *
     * @param BillOfLadingInput $Model The new BillOfLading resource
     *
     * @return BillOfLadingOutput
     */
    public function postCollection(BillOfLadingInput $Model): BillOfLadingOutput
    {
        return $this->request(
        'postBillOfLadingCollection',
        'POST',
        'api/shipments/bill_of_ladings',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves the collection of BillOfLading resources.
     *
     * @param string $consignmentUuid
     * @param array  $queries         options:
     *                                'page'	integer	The collection page number
     *                                'itemsPerPage'	integer	The number of items per page
     *                                'uuid'	string
     *                                'uuid[]'	string
     *                                'masterWaybill.uuid'	string
     *                                'masterWaybill.uuid[]'	string
     *                                'exists[masterWaybill]'	boolean
     *                                'id'	integer
     *                                'id[]'	array
     *                                'clientId'	integer
     *                                'clientId[]'	array
     *                                'clientCode'	string
     *                                'clientCode[]'	array
     *                                'reference'	string
     *                                'ucn'	string
     *                                'customsEntry'	string
     *                                'customsStatus'	string
     *                                'customsStatus[]'	array
     *                                'status'	string
     *                                'status[]'	array
     *                                'billOfLadingDelivery.deliveryService.deliveryServiceTypeCode'	string
     *                                'masterWaybill.reference'	string
     *                                'container.reference'	string
     *                                'order[id]'	string
     *                                'order[uuid]'	string
     *                                'order[clientId]'	string
     *                                'createTime[before]'	string
     *                                'createTime[strictly_before]'	string
     *                                'createTime[after]'	string
     *                                'createTime[strictly_after]'	string
     *                                'updateTime[before]'	string
     *                                'updateTime[strictly_before]'	string
     *                                'updateTime[after]'	string
     *                                'updateTime[strictly_after]'	string
     *
     * @return BillOfLadingOutput[]|null
     */
    public function getLinkableByConsignmentCollection(string $consignmentUuid, array $queries = []): ?array
    {
        return $this->request(
        'getLinkableByConsignmentBillOfLadingCollection',
        'GET',
        "api/shipments/bill_of_ladings/get_linkable_by_consignment/$consignmentUuid",
        null,
        $queries,
        []
        );
    }

    /**
     * Retrieves the collection of BillOfLading resources.
     *
     * @param string $containerUuid
     * @param array  $queries       options:
     *                              'page'	integer	The collection page number
     *                              'itemsPerPage'	integer	The number of items per page
     *                              'uuid'	string
     *                              'uuid[]'	string
     *                              'masterWaybill.uuid'	string
     *                              'masterWaybill.uuid[]'	string
     *                              'exists[masterWaybill]'	boolean
     *                              'id'	integer
     *                              'id[]'	array
     *                              'clientId'	integer
     *                              'clientId[]'	array
     *                              'clientCode'	string
     *                              'clientCode[]'	array
     *                              'reference'	string
     *                              'ucn'	string
     *                              'customsEntry'	string
     *                              'customsStatus'	string
     *                              'customsStatus[]'	array
     *                              'status'	string
     *                              'status[]'	array
     *                              'billOfLadingDelivery.deliveryService.deliveryServiceTypeCode'	string
     *                              'masterWaybill.reference'	string
     *                              'container.reference'	string
     *                              'order[id]'	string
     *                              'order[uuid]'	string
     *                              'order[clientId]'	string
     *                              'createTime[before]'	string
     *                              'createTime[strictly_before]'	string
     *                              'createTime[after]'	string
     *                              'createTime[strictly_after]'	string
     *                              'updateTime[before]'	string
     *                              'updateTime[strictly_before]'	string
     *                              'updateTime[after]'	string
     *                              'updateTime[strictly_after]'	string
     *
     * @return BillOfLadingOutput[]|null
     */
    public function getLinkableByContainerCollection(string $containerUuid, array $queries = []): ?array
    {
        return $this->request(
        'getLinkableByContainerBillOfLadingCollection',
        'GET',
        "api/shipments/bill_of_ladings/get_linkable_by_container/$containerUuid",
        null,
        $queries,
        []
        );
    }

    /**
     * Retrieves the collection of BillOfLading resources.
     *
     * @param string $masterWaybillUuid
     * @param array  $queries           options:
     *                                  'page'	integer	The collection page number
     *                                  'itemsPerPage'	integer	The number of items per page
     *                                  'uuid'	string
     *                                  'uuid[]'	string
     *                                  'masterWaybill.uuid'	string
     *                                  'masterWaybill.uuid[]'	string
     *                                  'exists[masterWaybill]'	boolean
     *                                  'id'	integer
     *                                  'id[]'	array
     *                                  'clientId'	integer
     *                                  'clientId[]'	array
     *                                  'clientCode'	string
     *                                  'clientCode[]'	array
     *                                  'reference'	string
     *                                  'ucn'	string
     *                                  'customsEntry'	string
     *                                  'customsStatus'	string
     *                                  'customsStatus[]'	array
     *                                  'status'	string
     *                                  'status[]'	array
     *                                  'billOfLadingDelivery.deliveryService.deliveryServiceTypeCode'	string
     *                                  'masterWaybill.reference'	string
     *                                  'container.reference'	string
     *                                  'order[id]'	string
     *                                  'order[uuid]'	string
     *                                  'order[clientId]'	string
     *                                  'createTime[before]'	string
     *                                  'createTime[strictly_before]'	string
     *                                  'createTime[after]'	string
     *                                  'createTime[strictly_after]'	string
     *                                  'updateTime[before]'	string
     *                                  'updateTime[strictly_before]'	string
     *                                  'updateTime[after]'	string
     *                                  'updateTime[strictly_after]'	string
     *
     * @return BillOfLadingOutput[]|null
     */
    public function getLinkableByMasterWaybillCollection(string $masterWaybillUuid, array $queries = []): ?array
    {
        return $this->request(
        'getLinkableByMasterWaybillBillOfLadingCollection',
        'GET',
        "api/shipments/bill_of_ladings/get_linkable_by_master_waybill/$masterWaybillUuid",
        null,
        $queries,
        []
        );
    }

    /**
     * Retrieves the collection of BillOfLading resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'uuid'	string
     *                       'uuid[]'	string
     *                       'masterWaybill.uuid'	string
     *                       'masterWaybill.uuid[]'	string
     *                       'exists[masterWaybill]'	boolean
     *                       'id'	integer
     *                       'id[]'	array
     *                       'clientId'	integer
     *                       'clientId[]'	array
     *                       'clientCode'	string
     *                       'clientCode[]'	array
     *                       'reference'	string
     *                       'ucn'	string
     *                       'customsEntry'	string
     *                       'customsStatus'	string
     *                       'customsStatus[]'	array
     *                       'status'	string
     *                       'status[]'	array
     *                       'billOfLadingDelivery.deliveryService.deliveryServiceTypeCode'	string
     *                       'masterWaybill.reference'	string
     *                       'container.reference'	string
     *                       'order[id]'	string
     *                       'order[uuid]'	string
     *                       'order[clientId]'	string
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'updateTime[before]'	string
     *                       'updateTime[strictly_before]'	string
     *                       'updateTime[after]'	string
     *                       'updateTime[strictly_after]'	string
     *
     * @return Statistics[]|null
     */
    public function statusStatisticsCollection(array $queries = []): ?array
    {
        return $this->request(
        'statusStatisticsBillOfLadingCollection',
        'GET',
        'api/shipments/bill_of_ladings/statistics',
        null,
        $queries,
        []
        );
    }

    /**
     * Retrieves a BillOfLading resource.
     *
     * @param string $uuid Resource identifier
     *
     * @return BillOfLadingOutput|null
     */
    public function getItem(string $uuid): ?BillOfLadingOutput
    {
        return $this->request(
        'getBillOfLadingItem',
        'GET',
        "api/shipments/bill_of_ladings/$uuid",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the BillOfLading resource.
     *
     * @param string            $uuid  Resource identifier
     * @param BillOfLadingInput $Model The updated BillOfLading resource
     *
     * @return BillOfLadingOutput
     */
    public function putItem(string $uuid, BillOfLadingInput $Model): BillOfLadingOutput
    {
        return $this->request(
        'putBillOfLadingItem',
        'PUT',
        "api/shipments/bill_of_ladings/$uuid",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the BillOfLading resource.
     *
     * @param string $uuid Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $uuid): mixed
    {
        return $this->request(
        'deleteBillOfLadingItem',
        'DELETE',
        "api/shipments/bill_of_ladings/$uuid",
        null,
        [],
        []
        );
    }
}
