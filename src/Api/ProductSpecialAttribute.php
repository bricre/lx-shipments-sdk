<?php

namespace LogisticsX\Shipments\Api;

use LogisticsX\Shipments\Model\ProductSpecialAttribute\ProductSpecialAttributeInput;
use LogisticsX\Shipments\Model\ProductSpecialAttribute\ProductSpecialAttributeOutput;

class ProductSpecialAttribute extends AbstractAPI
{
    /**
     * Retrieves the collection of ProductSpecialAttribute resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'code'	string
     *                       'code[]'	array
     *                       'name'	string
     *                       'name[]'	array
     *                       'order[code]'	string
     *                       'order[name]'	string
     *
     * @return ProductSpecialAttributeOutput[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getProductSpecialAttributeCollection',
        'GET',
        'api/shipments/product_special_attributes',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a ProductSpecialAttribute resource.
     *
     * @param ProductSpecialAttributeInput $Model The new ProductSpecialAttribute
     *                                            resource
     *
     * @return ProductSpecialAttributeOutput
     */
    public function postCollection(ProductSpecialAttributeInput $Model): ProductSpecialAttributeOutput
    {
        return $this->request(
        'postProductSpecialAttributeCollection',
        'POST',
        'api/shipments/product_special_attributes',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a ProductSpecialAttribute resource.
     *
     * @param string $code Resource identifier
     *
     * @return ProductSpecialAttributeOutput|null
     */
    public function getItem(string $code): ?ProductSpecialAttributeOutput
    {
        return $this->request(
        'getProductSpecialAttributeItem',
        'GET',
        "api/shipments/product_special_attributes/$code",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the ProductSpecialAttribute resource.
     *
     * @param string                       $code  Resource identifier
     * @param ProductSpecialAttributeInput $Model The updated ProductSpecialAttribute
     *                                            resource
     *
     * @return ProductSpecialAttributeOutput
     */
    public function putItem(string $code, ProductSpecialAttributeInput $Model): ProductSpecialAttributeOutput
    {
        return $this->request(
        'putProductSpecialAttributeItem',
        'PUT',
        "api/shipments/product_special_attributes/$code",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the ProductSpecialAttribute resource.
     *
     * @param string $code Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $code): mixed
    {
        return $this->request(
        'deleteProductSpecialAttributeItem',
        'DELETE',
        "api/shipments/product_special_attributes/$code",
        null,
        [],
        []
        );
    }
}
