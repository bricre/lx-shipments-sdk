<?php

namespace LogisticsX\Shipments\Api;

use LogisticsX\Shipments\Model\Consignment\ConsignmentCollection;
use LogisticsX\Shipments\Model\Consignment\ConsignmentInput;
use LogisticsX\Shipments\Model\Consignment\ConsignmentOutput;
use LogisticsX\Shipments\Model\Consignment\Customs\Clearance;
use LogisticsX\Shipments\Model\Consignment\Statistics;
use LogisticsX\Shipments\Model\Consignment\Stock\In;

class Consignment extends AbstractAPI
{
    /**
     * Retrieves the collection of Consignment resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'uuid'	string
     *                       'uuid[]'	string
     *                       'masterWaybill.uuid'	string
     *                       'masterWaybill.uuid[]'	string
     *                       'billOfLading.uuid'	string
     *                       'billOfLading.uuid[]'	string
     *                       'container.uuid'	string
     *                       'container.uuid[]'	string
     *                       'id'	integer
     *                       'id[]'	array
     *                       'reference'	string
     *                       'clientId'	integer
     *                       'clientId[]'	array
     *                       'clientCode'	string
     *                       'clientReference'	string
     *                       'additionalReference'	string
     *                       'billOfLading.reference'	string
     *                       'importerVat'	string
     *                       'productSpecialAttributes'	string
     *                       'contact'	string
     *                       'businessName'	string
     *                       'addressLine1'	string
     *                       'addressLine2'	string
     *                       'addressLine3'	string
     *                       'city'	string
     *                       'county'	string
     *                       'postcode'	string
     *                       'telephone'	string
     *                       'email'	string
     *                       'status'	string
     *                       'status[]'	array
     *                       'masterWaybill.reference'	string
     *                       'outStockOrder.deliveryReference'	string
     *                       'container.reference'	string
     *                       'ucn'	string
     *                       'billOfLading.customsStatus'	string
     *                       'billOfLading.customsStatus[]'	array
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'updateTime[before]'	string
     *                       'updateTime[strictly_before]'	string
     *                       'updateTime[after]'	string
     *                       'updateTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[uuid]'	string
     *                       'order[reference]'	string
     *                       'order[clientId]'	string
     *                       'order[clientCode]'	string
     *                       'order[clientReference]'	string
     *                       'order[additionalReference]'	string
     *                       'order[billOfLading.reference]'	string
     *                       'order[productSpecialAttributes]'	string
     *                       'order[contact]'	string
     *                       'order[businessName]'	string
     *                       'order[addressLine1]'	string
     *                       'order[addressLine2]'	string
     *                       'order[addressLine3]'	string
     *                       'order[city]'	string
     *                       'order[county]'	string
     *                       'order[postcode]'	string
     *                       'order[telephone]'	string
     *                       'order[email]'	string
     *                       'order[status]'	string
     *                       'order[createTime]'	string
     *                       'order[updateTime]'	string
     *
     * @return ConsignmentCollection[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getConsignmentCollection',
        'GET',
        'api/shipments/consignments',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a Consignment resource.
     *
     * @param ConsignmentInput $Model The new Consignment resource
     *
     * @return ConsignmentOutput
     */
    public function postCollection(ConsignmentInput $Model): ConsignmentOutput
    {
        return $this->request(
        'postConsignmentCollection',
        'POST',
        'api/shipments/consignments',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves the collection of Consignment resources.
     *
     * @param string $billOfLadingUuid
     * @param array  $queries          options:
     *                                 'page'	integer	The collection page number
     *                                 'itemsPerPage'	integer	The number of items per page
     *                                 'uuid'	string
     *                                 'uuid[]'	string
     *                                 'masterWaybill.uuid'	string
     *                                 'masterWaybill.uuid[]'	string
     *                                 'billOfLading.uuid'	string
     *                                 'billOfLading.uuid[]'	string
     *                                 'container.uuid'	string
     *                                 'container.uuid[]'	string
     *                                 'id'	integer
     *                                 'id[]'	array
     *                                 'reference'	string
     *                                 'clientId'	integer
     *                                 'clientId[]'	array
     *                                 'clientCode'	string
     *                                 'clientReference'	string
     *                                 'additionalReference'	string
     *                                 'billOfLading.reference'	string
     *                                 'importerVat'	string
     *                                 'productSpecialAttributes'	string
     *                                 'contact'	string
     *                                 'businessName'	string
     *                                 'addressLine1'	string
     *                                 'addressLine2'	string
     *                                 'addressLine3'	string
     *                                 'city'	string
     *                                 'county'	string
     *                                 'postcode'	string
     *                                 'telephone'	string
     *                                 'email'	string
     *                                 'status'	string
     *                                 'status[]'	array
     *                                 'masterWaybill.reference'	string
     *                                 'outStockOrder.deliveryReference'	string
     *                                 'container.reference'	string
     *                                 'ucn'	string
     *                                 'billOfLading.customsStatus'	string
     *                                 'billOfLading.customsStatus[]'	array
     *                                 'createTime[before]'	string
     *                                 'createTime[strictly_before]'	string
     *                                 'createTime[after]'	string
     *                                 'createTime[strictly_after]'	string
     *                                 'updateTime[before]'	string
     *                                 'updateTime[strictly_before]'	string
     *                                 'updateTime[after]'	string
     *                                 'updateTime[strictly_after]'	string
     *                                 'order[id]'	string
     *                                 'order[uuid]'	string
     *                                 'order[reference]'	string
     *                                 'order[clientId]'	string
     *                                 'order[clientCode]'	string
     *                                 'order[clientReference]'	string
     *                                 'order[additionalReference]'	string
     *                                 'order[billOfLading.reference]'	string
     *                                 'order[productSpecialAttributes]'	string
     *                                 'order[contact]'	string
     *                                 'order[businessName]'	string
     *                                 'order[addressLine1]'	string
     *                                 'order[addressLine2]'	string
     *                                 'order[addressLine3]'	string
     *                                 'order[city]'	string
     *                                 'order[county]'	string
     *                                 'order[postcode]'	string
     *                                 'order[telephone]'	string
     *                                 'order[email]'	string
     *                                 'order[status]'	string
     *                                 'order[createTime]'	string
     *                                 'order[updateTime]'	string
     *
     * @return ConsignmentOutput[]|null
     */
    public function getLinkableByBillOfLadingCollection(string $billOfLadingUuid, array $queries = []): ?array
    {
        return $this->request(
        'getLinkableByBillOfLadingConsignmentCollection',
        'GET',
        "api/shipments/consignments/get_linkable_by_bill_of_lading/$billOfLadingUuid",
        null,
        $queries,
        []
        );
    }

    /**
     * Retrieves the collection of Consignment resources.
     *
     * @param string $masterWaybillUuid
     * @param array  $queries           options:
     *                                  'page'	integer	The collection page number
     *                                  'itemsPerPage'	integer	The number of items per page
     *                                  'uuid'	string
     *                                  'uuid[]'	string
     *                                  'masterWaybill.uuid'	string
     *                                  'masterWaybill.uuid[]'	string
     *                                  'billOfLading.uuid'	string
     *                                  'billOfLading.uuid[]'	string
     *                                  'container.uuid'	string
     *                                  'container.uuid[]'	string
     *                                  'id'	integer
     *                                  'id[]'	array
     *                                  'reference'	string
     *                                  'clientId'	integer
     *                                  'clientId[]'	array
     *                                  'clientCode'	string
     *                                  'clientReference'	string
     *                                  'additionalReference'	string
     *                                  'billOfLading.reference'	string
     *                                  'importerVat'	string
     *                                  'productSpecialAttributes'	string
     *                                  'contact'	string
     *                                  'businessName'	string
     *                                  'addressLine1'	string
     *                                  'addressLine2'	string
     *                                  'addressLine3'	string
     *                                  'city'	string
     *                                  'county'	string
     *                                  'postcode'	string
     *                                  'telephone'	string
     *                                  'email'	string
     *                                  'status'	string
     *                                  'status[]'	array
     *                                  'masterWaybill.reference'	string
     *                                  'outStockOrder.deliveryReference'	string
     *                                  'container.reference'	string
     *                                  'ucn'	string
     *                                  'billOfLading.customsStatus'	string
     *                                  'billOfLading.customsStatus[]'	array
     *                                  'createTime[before]'	string
     *                                  'createTime[strictly_before]'	string
     *                                  'createTime[after]'	string
     *                                  'createTime[strictly_after]'	string
     *                                  'updateTime[before]'	string
     *                                  'updateTime[strictly_before]'	string
     *                                  'updateTime[after]'	string
     *                                  'updateTime[strictly_after]'	string
     *                                  'order[id]'	string
     *                                  'order[uuid]'	string
     *                                  'order[reference]'	string
     *                                  'order[clientId]'	string
     *                                  'order[clientCode]'	string
     *                                  'order[clientReference]'	string
     *                                  'order[additionalReference]'	string
     *                                  'order[billOfLading.reference]'	string
     *                                  'order[productSpecialAttributes]'	string
     *                                  'order[contact]'	string
     *                                  'order[businessName]'	string
     *                                  'order[addressLine1]'	string
     *                                  'order[addressLine2]'	string
     *                                  'order[addressLine3]'	string
     *                                  'order[city]'	string
     *                                  'order[county]'	string
     *                                  'order[postcode]'	string
     *                                  'order[telephone]'	string
     *                                  'order[email]'	string
     *                                  'order[status]'	string
     *                                  'order[createTime]'	string
     *                                  'order[updateTime]'	string
     *
     * @return ConsignmentOutput[]|null
     */
    public function getLinkableByMasterWaybillCollection(string $masterWaybillUuid, array $queries = []): ?array
    {
        return $this->request(
        'getLinkableByMasterWaybillConsignmentCollection',
        'GET',
        "api/shipments/consignments/get_linkable_by_master_waybill/$masterWaybillUuid",
        null,
        $queries,
        []
        );
    }

    /**
     * Retrieves the collection of Consignment resources.
     *
     * @param array $queries options:
     *                       'uuid'	string
     *                       'uuid[]'	string
     *                       'masterWaybill.uuid'	string
     *                       'masterWaybill.uuid[]'	string
     *                       'billOfLading.uuid'	string
     *                       'billOfLading.uuid[]'	string
     *                       'container.uuid'	string
     *                       'container.uuid[]'	string
     *                       'id'	integer
     *                       'id[]'	array
     *                       'reference'	string
     *                       'clientId'	integer
     *                       'clientId[]'	array
     *                       'clientCode'	string
     *                       'clientReference'	string
     *                       'additionalReference'	string
     *                       'billOfLading.reference'	string
     *                       'importerVat'	string
     *                       'productSpecialAttributes'	string
     *                       'contact'	string
     *                       'businessName'	string
     *                       'addressLine1'	string
     *                       'addressLine2'	string
     *                       'addressLine3'	string
     *                       'city'	string
     *                       'county'	string
     *                       'postcode'	string
     *                       'telephone'	string
     *                       'email'	string
     *                       'status'	string
     *                       'status[]'	array
     *                       'masterWaybill.reference'	string
     *                       'outStockOrder.deliveryReference'	string
     *                       'container.reference'	string
     *                       'ucn'	string
     *                       'billOfLading.customsStatus'	string
     *                       'billOfLading.customsStatus[]'	array
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'updateTime[before]'	string
     *                       'updateTime[strictly_before]'	string
     *                       'updateTime[after]'	string
     *                       'updateTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[uuid]'	string
     *                       'order[reference]'	string
     *                       'order[clientId]'	string
     *                       'order[clientCode]'	string
     *                       'order[clientReference]'	string
     *                       'order[additionalReference]'	string
     *                       'order[billOfLading.reference]'	string
     *                       'order[productSpecialAttributes]'	string
     *                       'order[contact]'	string
     *                       'order[businessName]'	string
     *                       'order[addressLine1]'	string
     *                       'order[addressLine2]'	string
     *                       'order[addressLine3]'	string
     *                       'order[city]'	string
     *                       'order[county]'	string
     *                       'order[postcode]'	string
     *                       'order[telephone]'	string
     *                       'order[email]'	string
     *                       'order[status]'	string
     *                       'order[createTime]'	string
     *                       'order[updateTime]'	string
     *
     * @return Statistics[]|null
     */
    public function statusStatisticsCollection(array $queries = []): ?array
    {
        return $this->request(
        'statusStatisticsConsignmentCollection',
        'GET',
        'api/shipments/consignments/statistics',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a Consignment resource.
     *
     * @return ConsignmentOutput
     */
    public function uploadCollection(): ConsignmentOutput
    {
        return $this->request(
        'uploadConsignmentCollection',
        'POST',
        'api/shipments/consignments/upload',
        null,
        [],
        []
        );
    }

    /**
     * Retrieves a Consignment resource.
     *
     * @param string $uuid Resource identifier
     *
     * @return ConsignmentOutput|null
     */
    public function getItem(string $uuid): ?ConsignmentOutput
    {
        return $this->request(
        'getConsignmentItem',
        'GET',
        "api/shipments/consignments/$uuid",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the Consignment resource.
     *
     * @param string           $uuid  Resource identifier
     * @param ConsignmentInput $Model The updated Consignment resource
     *
     * @return ConsignmentOutput
     */
    public function putItem(string $uuid, ConsignmentInput $Model): ConsignmentOutput
    {
        return $this->request(
        'putConsignmentItem',
        'PUT',
        "api/shipments/consignments/$uuid",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the Consignment resource.
     *
     * @param string $uuid Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $uuid): mixed
    {
        return $this->request(
        'deleteConsignmentItem',
        'DELETE',
        "api/shipments/consignments/$uuid",
        null,
        [],
        []
        );
    }

    /**
     * Retrieves the collection of Consignment resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'uuid'	string
     *                       'uuid[]'	string
     *                       'masterWaybill.uuid'	string
     *                       'masterWaybill.uuid[]'	string
     *                       'billOfLading.uuid'	string
     *                       'billOfLading.uuid[]'	string
     *                       'container.uuid'	string
     *                       'container.uuid[]'	string
     *                       'id'	integer
     *                       'id[]'	array
     *                       'reference'	string
     *                       'clientId'	integer
     *                       'clientId[]'	array
     *                       'clientCode'	string
     *                       'clientReference'	string
     *                       'additionalReference'	string
     *                       'billOfLading.reference'	string
     *                       'importerVat'	string
     *                       'productSpecialAttributes'	string
     *                       'contact'	string
     *                       'businessName'	string
     *                       'addressLine1'	string
     *                       'addressLine2'	string
     *                       'addressLine3'	string
     *                       'city'	string
     *                       'county'	string
     *                       'postcode'	string
     *                       'telephone'	string
     *                       'email'	string
     *                       'status'	string
     *                       'status[]'	array
     *                       'masterWaybill.reference'	string
     *                       'outStockOrder.deliveryReference'	string
     *                       'container.reference'	string
     *                       'ucn'	string
     *                       'billOfLading.customsStatus'	string
     *                       'billOfLading.customsStatus[]'	array
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'updateTime[before]'	string
     *                       'updateTime[strictly_before]'	string
     *                       'updateTime[after]'	string
     *                       'updateTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[uuid]'	string
     *                       'order[reference]'	string
     *                       'order[clientId]'	string
     *                       'order[clientCode]'	string
     *                       'order[clientReference]'	string
     *                       'order[additionalReference]'	string
     *                       'order[billOfLading.reference]'	string
     *                       'order[productSpecialAttributes]'	string
     *                       'order[contact]'	string
     *                       'order[businessName]'	string
     *                       'order[addressLine1]'	string
     *                       'order[addressLine2]'	string
     *                       'order[addressLine3]'	string
     *                       'order[city]'	string
     *                       'order[county]'	string
     *                       'order[postcode]'	string
     *                       'order[telephone]'	string
     *                       'order[email]'	string
     *                       'order[status]'	string
     *                       'order[createTime]'	string
     *                       'order[updateTime]'	string
     *
     * @return Clearance[]|null
     */
    public function customsClearanceCollection(array $queries = []): ?array
    {
        return $this->request(
        'customsClearanceConsignmentCollection',
        'GET',
        'api/shipments/operation/customs_clearance',
        null,
        $queries,
        []
        );
    }

    /**
     * Retrieves the collection of Consignment resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'uuid'	string
     *                       'uuid[]'	string
     *                       'masterWaybill.uuid'	string
     *                       'masterWaybill.uuid[]'	string
     *                       'billOfLading.uuid'	string
     *                       'billOfLading.uuid[]'	string
     *                       'container.uuid'	string
     *                       'container.uuid[]'	string
     *                       'id'	integer
     *                       'id[]'	array
     *                       'reference'	string
     *                       'clientId'	integer
     *                       'clientId[]'	array
     *                       'clientCode'	string
     *                       'clientReference'	string
     *                       'additionalReference'	string
     *                       'billOfLading.reference'	string
     *                       'importerVat'	string
     *                       'productSpecialAttributes'	string
     *                       'contact'	string
     *                       'businessName'	string
     *                       'addressLine1'	string
     *                       'addressLine2'	string
     *                       'addressLine3'	string
     *                       'city'	string
     *                       'county'	string
     *                       'postcode'	string
     *                       'telephone'	string
     *                       'email'	string
     *                       'status'	string
     *                       'status[]'	array
     *                       'masterWaybill.reference'	string
     *                       'outStockOrder.deliveryReference'	string
     *                       'container.reference'	string
     *                       'ucn'	string
     *                       'billOfLading.customsStatus'	string
     *                       'billOfLading.customsStatus[]'	array
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'updateTime[before]'	string
     *                       'updateTime[strictly_before]'	string
     *                       'updateTime[after]'	string
     *                       'updateTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[uuid]'	string
     *                       'order[reference]'	string
     *                       'order[clientId]'	string
     *                       'order[clientCode]'	string
     *                       'order[clientReference]'	string
     *                       'order[additionalReference]'	string
     *                       'order[billOfLading.reference]'	string
     *                       'order[productSpecialAttributes]'	string
     *                       'order[contact]'	string
     *                       'order[businessName]'	string
     *                       'order[addressLine1]'	string
     *                       'order[addressLine2]'	string
     *                       'order[addressLine3]'	string
     *                       'order[city]'	string
     *                       'order[county]'	string
     *                       'order[postcode]'	string
     *                       'order[telephone]'	string
     *                       'order[email]'	string
     *                       'order[status]'	string
     *                       'order[createTime]'	string
     *                       'order[updateTime]'	string
     *
     * @return In[]|null
     */
    public function stockInCollection(array $queries = []): ?array
    {
        return $this->request(
        'stockInConsignmentCollection',
        'GET',
        'api/shipments/operation/stock_in',
        null,
        $queries,
        []
        );
    }
}
