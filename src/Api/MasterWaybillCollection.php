<?php

namespace LogisticsX\Shipments\Api;

use LogisticsX\Shipments\Model\MasterWaybillCollection\MasterWaybillCollectionInput;
use LogisticsX\Shipments\Model\MasterWaybillCollection\MasterWaybillCollectionOutput;
use LogisticsX\Shipments\Model\MasterWaybillCollection\MasterWaybillCollectionUpdate;

class MasterWaybillCollection extends AbstractAPI
{
    /**
     * Creates a MasterWaybillCollection resource.
     *
     * @param MasterWaybillCollectionInput $Model The new MasterWaybillCollection
     *                                            resource
     *
     * @return MasterWaybillCollectionOutput
     */
    public function postCollection(MasterWaybillCollectionInput $Model): MasterWaybillCollectionOutput
    {
        return $this->request(
        'postMasterWaybillCollectionCollection',
        'POST',
        'api/shipments/master_waybill_collections',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a MasterWaybillCollection resource.
     *
     * @param string $id Resource identifier
     *
     * @return MasterWaybillCollectionOutput|null
     */
    public function getItem(string $id): ?MasterWaybillCollectionOutput
    {
        return $this->request(
        'getMasterWaybillCollectionItem',
        'GET',
        "api/shipments/master_waybill_collections/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the MasterWaybillCollection resource.
     *
     * @param string                        $id    Resource identifier
     * @param MasterWaybillCollectionUpdate $Model The updated MasterWaybillCollection
     *                                             resource
     *
     * @return MasterWaybillCollectionUpdate
     */
    public function putItem(string $id, MasterWaybillCollectionUpdate $Model): MasterWaybillCollectionUpdate
    {
        return $this->request(
        'putMasterWaybillCollectionItem',
        'PUT',
        "api/shipments/master_waybill_collections/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }
}
