<?php

namespace LogisticsX\Shipments\Api;

use LogisticsX\Shipments\Model\ConsignmentCartonProduct\ConsignmentCartonProduct\SummaryByConsignment;
use LogisticsX\Shipments\Model\ConsignmentCartonProduct\ConsignmentCartonProductInput\ConsignmentInput;
use LogisticsX\Shipments\Model\ConsignmentCartonProduct\ConsignmentCartonProductOutput;

class ConsignmentCartonProduct extends AbstractAPI
{
    /**
     * Retrieves the collection of ConsignmentCartonProduct resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'consignmentCarton.uuid'	string
     *                       'consignmentCarton.uuid[]'	string
     *                       'consignmentCarton.consignment.uuid'	string
     *                       'consignmentCarton.consignment.uuid[]'	string
     *                       'consignmentCarton.consignment.billOfLading.uuid'	string
     *                       'consignmentCarton.consignment.billOfLading.uuid[]'	string
     *                       'id'	integer
     *                       'id[]'	array
     *                       'consignmentCarton.id'	integer
     *                       'consignmentCarton.reference'	string
     *                       'consignmentCarton.consignment.id'	integer
     *                       'consignmentCarton.consignment.id[]'	array
     *                       'name'	string
     *                       'nameChinese'	string
     *                       'commodityCode'	string
     *                       'price'	string
     *                       'price[]'	array
     *                       'quantity'	integer
     *                       'quantity[]'	array
     *                       'order[id]'	string
     *                       'order[consignmentCarton.id]'	string
     *                       'order[consignmentCarton.reference]'	string
     *                       'order[consignmentCarton.uuid]'	string
     *                       'order[consignmentCarton.consignment.uuid]'	string
     *                       'order[name]'	string
     *                       'order[nameChinese]'	string
     *                       'order[commodityCode]'	string
     *                       'order[price]'	string
     *                       'order[quantity]'	string
     *
     * @return ConsignmentCartonProductOutput[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getConsignmentCartonProductCollection',
        'GET',
        'api/shipments/consignment_carton_products',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a ConsignmentCartonProduct resource.
     *
     * @param ConsignmentInput $Model The new ConsignmentCartonProduct resource
     *
     * @return ConsignmentCartonProductOutput
     */
    public function postCollection(ConsignmentInput $Model): ConsignmentCartonProductOutput
    {
        return $this->request(
        'postConsignmentCartonProductCollection',
        'POST',
        'api/shipments/consignment_carton_products',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves the collection of ConsignmentCartonProduct resources.
     *
     * @param string $consignmentUuid
     * @param array  $queries         options:
     *                                'page'	integer	The collection page number
     *                                'itemsPerPage'	integer	The number of items per page
     *                                'consignmentCarton.uuid'	string
     *                                'consignmentCarton.uuid[]'	string
     *                                'consignmentCarton.consignment.uuid'	string
     *                                'consignmentCarton.consignment.uuid[]'	string
     *                                'consignmentCarton.consignment.billOfLading.uuid'	string
     *                                'consignmentCarton.consignment.billOfLading.uuid[]'	string
     *                                'id'	integer
     *                                'id[]'	array
     *                                'consignmentCarton.id'	integer
     *                                'consignmentCarton.reference'	string
     *                                'consignmentCarton.consignment.id'	integer
     *                                'consignmentCarton.consignment.id[]'	array
     *                                'name'	string
     *                                'nameChinese'	string
     *                                'commodityCode'	string
     *                                'price'	string
     *                                'price[]'	array
     *                                'quantity'	integer
     *                                'quantity[]'	array
     *                                'order[id]'	string
     *                                'order[consignmentCarton.id]'	string
     *                                'order[consignmentCarton.reference]'	string
     *                                'order[consignmentCarton.uuid]'	string
     *                                'order[consignmentCarton.consignment.uuid]'	string
     *                                'order[name]'	string
     *                                'order[nameChinese]'	string
     *                                'order[commodityCode]'	string
     *                                'order[price]'	string
     *                                'order[quantity]'	string
     *
     * @return SummaryByConsignment[]|null
     */
    public function summaryByConsignmentCollection(string $consignmentUuid, array $queries = []): ?array
    {
        return $this->request(
        'summaryByConsignmentConsignmentCartonProductCollection',
        'GET',
        "api/shipments/consignment_carton_products/summary_by_consignment/$consignmentUuid",
        null,
        $queries,
        []
        );
    }

    /**
     * Retrieves a ConsignmentCartonProduct resource.
     *
     * @param string $id Resource identifier
     *
     * @return ConsignmentCartonProductOutput|null
     */
    public function getItem(string $id): ?ConsignmentCartonProductOutput
    {
        return $this->request(
        'getConsignmentCartonProductItem',
        'GET',
        "api/shipments/consignment_carton_products/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the ConsignmentCartonProduct resource.
     *
     * @param string           $id    Resource identifier
     * @param ConsignmentInput $Model The updated ConsignmentCartonProduct resource
     *
     * @return ConsignmentCartonProductOutput
     */
    public function putItem(string $id, ConsignmentInput $Model): ConsignmentCartonProductOutput
    {
        return $this->request(
        'putConsignmentCartonProductItem',
        'PUT',
        "api/shipments/consignment_carton_products/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the ConsignmentCartonProduct resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteConsignmentCartonProductItem',
        'DELETE',
        "api/shipments/consignment_carton_products/$id",
        null,
        [],
        []
        );
    }
}
