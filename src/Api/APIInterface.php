<?php

namespace LogisticsX\Shipments\Api;

use OpenAPI\Runtime\APIInterface as BaseClass;

interface APIInterface extends BaseClass
{
}
