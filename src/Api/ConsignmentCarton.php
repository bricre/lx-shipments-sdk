<?php

namespace LogisticsX\Shipments\Api;

use LogisticsX\Shipments\Model\ConsignmentCarton\ConsignmentCartonInput;
use LogisticsX\Shipments\Model\ConsignmentCarton\ConsignmentCartonOutput;

class ConsignmentCarton extends AbstractAPI
{
    /**
     * Retrieves the collection of ConsignmentCarton resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'uuid'	string
     *                       'uuid[]'	string
     *                       'consignment.uuid'	string
     *                       'consignment.uuid[]'	string
     *                       'consignment.container.uuid'	string
     *                       'consignment.container.uuid[]'	string
     *                       'id'	integer
     *                       'id[]'	array
     *                       'consignment.id'	integer
     *                       'consignment.id[]'	array
     *                       'reference'	string
     *                       'additionalReference'	string
     *                       'deliveryReference'	string
     *                       'order[id]'	string
     *                       'order[uuid]'	string
     *                       'order[consignment.uuid]'	string
     *                       'order[consignment.id]'	string
     *                       'order[reference]'	string
     *                       'order[additionalReference]'	string
     *                       'order[deliveryReference]'	string
     *
     * @return ConsignmentCartonOutput[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getConsignmentCartonCollection',
        'GET',
        'api/shipments/consignment_cartons',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a ConsignmentCarton resource.
     *
     * @param ConsignmentCartonInput $Model The new ConsignmentCarton resource
     *
     * @return ConsignmentCartonOutput
     */
    public function postCollection(ConsignmentCartonInput $Model): ConsignmentCartonOutput
    {
        return $this->request(
        'postConsignmentCartonCollection',
        'POST',
        'api/shipments/consignment_cartons',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a ConsignmentCarton resource.
     *
     * @param string $uuid Resource identifier
     *
     * @return ConsignmentCartonOutput|null
     */
    public function getItem(string $uuid): ?ConsignmentCartonOutput
    {
        return $this->request(
        'getConsignmentCartonItem',
        'GET',
        "api/shipments/consignment_cartons/$uuid",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the ConsignmentCarton resource.
     *
     * @param string                 $uuid  Resource identifier
     * @param ConsignmentCartonInput $Model The updated ConsignmentCarton resource
     *
     * @return ConsignmentCartonOutput
     */
    public function putItem(string $uuid, ConsignmentCartonInput $Model): ConsignmentCartonOutput
    {
        return $this->request(
        'putConsignmentCartonItem',
        'PUT',
        "api/shipments/consignment_cartons/$uuid",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the ConsignmentCarton resource.
     *
     * @param string $uuid Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $uuid): mixed
    {
        return $this->request(
        'deleteConsignmentCartonItem',
        'DELETE',
        "api/shipments/consignment_cartons/$uuid",
        null,
        [],
        []
        );
    }
}
