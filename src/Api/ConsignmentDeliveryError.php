<?php

namespace LogisticsX\Shipments\Api;

use LogisticsX\Shipments\Model\ConsignmentDeliveryError\ConsignmentDeliveryErrorInput;

class ConsignmentDeliveryError extends AbstractAPI
{
    /**
     * Retrieves the collection of ConsignmentDeliveryError resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *
     * @return ConsignmentDeliveryErrorInput[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getConsignmentDeliveryErrorCollection',
        'GET',
        'api/shipments/consignment_delivery_errors',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a ConsignmentDeliveryError resource.
     *
     * @param ConsignmentDeliveryErrorInput $Model The new ConsignmentDeliveryError
     *                                             resource
     *
     * @return ConsignmentDeliveryErrorInput
     */
    public function postCollection(ConsignmentDeliveryErrorInput $Model): ConsignmentDeliveryErrorInput
    {
        return $this->request(
        'postConsignmentDeliveryErrorCollection',
        'POST',
        'api/shipments/consignment_delivery_errors',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a ConsignmentDeliveryError resource.
     *
     * @param string $consignmentDelivery Resource identifier
     *
     * @return ConsignmentDeliveryErrorInput|null
     */
    public function getItem(string $consignmentDelivery): ?ConsignmentDeliveryErrorInput
    {
        return $this->request(
        'getConsignmentDeliveryErrorItem',
        'GET',
        "api/shipments/consignment_delivery_errors/$consignmentDelivery",
        null,
        [],
        []
        );
    }
}
