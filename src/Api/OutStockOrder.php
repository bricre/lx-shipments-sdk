<?php

namespace LogisticsX\Shipments\Api;

use LogisticsX\Shipments\Model\OutStockOrder\OutStockOrderInput;
use LogisticsX\Shipments\Model\OutStockOrder\OutStockOrderOutput;

class OutStockOrder extends AbstractAPI
{
    /**
     * Retrieves the collection of OutStockOrder resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'id'	integer
     *                       'id[]'	array
     *                       'uuid'	string
     *                       'deliveryReference'	string
     *                       'order[id]'	string
     *                       'order[uuid]'	string
     *                       'order[deliveryReference]'	string
     *
     * @return OutStockOrderOutput[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getOutStockOrderCollection',
        'GET',
        'api/shipments/out_stock_orders',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a OutStockOrder resource.
     *
     * @param OutStockOrderInput $Model The new OutStockOrder resource
     *
     * @return OutStockOrderOutput
     */
    public function postCollection(OutStockOrderInput $Model): OutStockOrderOutput
    {
        return $this->request(
        'postOutStockOrderCollection',
        'POST',
        'api/shipments/out_stock_orders',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Creates a OutStockOrder resource.
     *
     * @param array $data
     *
     * @return OutStockOrderOutput
     */
    public function create_deliveryCollection(array $data = []): OutStockOrderOutput
    {
        return $this->request(
        'create_deliveryOutStockOrderCollection',
        'POST',
        'api/shipments/out_stock_orders/create_delivery',
        $data,
        [],
        []
        );
    }

    /**
     * Retrieves a OutStockOrder resource.
     *
     * @param string $uuid Resource identifier
     *
     * @return OutStockOrderOutput|null
     */
    public function getItem(string $uuid): ?OutStockOrderOutput
    {
        return $this->request(
        'getOutStockOrderItem',
        'GET',
        "api/shipments/out_stock_orders/$uuid",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the OutStockOrder resource.
     *
     * @param string             $uuid  Resource identifier
     * @param OutStockOrderInput $Model The updated OutStockOrder resource
     *
     * @return OutStockOrderOutput
     */
    public function putItem(string $uuid, OutStockOrderInput $Model): OutStockOrderOutput
    {
        return $this->request(
        'putOutStockOrderItem',
        'PUT',
        "api/shipments/out_stock_orders/$uuid",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the OutStockOrder resource.
     *
     * @param string $uuid Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $uuid): mixed
    {
        return $this->request(
        'deleteOutStockOrderItem',
        'DELETE',
        "api/shipments/out_stock_orders/$uuid",
        null,
        [],
        []
        );
    }
}
