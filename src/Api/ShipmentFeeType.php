<?php

namespace LogisticsX\Shipments\Api;

use LogisticsX\Shipments\Model\ShipmentFeeType\ShipmentFeeTypeInput;
use LogisticsX\Shipments\Model\ShipmentFeeType\ShipmentFeeTypeOutput;

class ShipmentFeeType extends AbstractAPI
{
    /**
     * Retrieves the collection of ShipmentFeeType resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'name'	string
     *                       'name[]'	array
     *                       'order[name]'	string
     *
     * @return ShipmentFeeTypeOutput[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getShipmentFeeTypeCollection',
        'GET',
        'api/shipments/shipment_fee_types',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a ShipmentFeeType resource.
     *
     * @param ShipmentFeeTypeInput $Model The new ShipmentFeeType resource
     *
     * @return ShipmentFeeTypeOutput
     */
    public function postCollection(ShipmentFeeTypeInput $Model): ShipmentFeeTypeOutput
    {
        return $this->request(
        'postShipmentFeeTypeCollection',
        'POST',
        'api/shipments/shipment_fee_types',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a ShipmentFeeType resource.
     *
     * @param string $name Resource identifier
     *
     * @return ShipmentFeeTypeOutput|null
     */
    public function getItem(string $name): ?ShipmentFeeTypeOutput
    {
        return $this->request(
        'getShipmentFeeTypeItem',
        'GET',
        "api/shipments/shipment_fee_types/$name",
        null,
        [],
        []
        );
    }

    /**
     * Removes the ShipmentFeeType resource.
     *
     * @param string $name Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $name): mixed
    {
        return $this->request(
        'deleteShipmentFeeTypeItem',
        'DELETE',
        "api/shipments/shipment_fee_types/$name",
        null,
        [],
        []
        );
    }
}
