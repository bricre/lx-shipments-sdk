<?php

namespace LogisticsX\Shipments\Api;

use LogisticsX\Shipments\Model\ContainerCollection\ContainerCollection\CreateMasterWaybill;
use LogisticsX\Shipments\Model\ContainerCollection\ContainerCollectionOutput;
use LogisticsX\Shipments\Model\ContainerCollection\ContainerCollectionUpdate;

class ContainerCollection extends AbstractAPI
{
    /**
     * Creates a ContainerCollection resource.
     *
     * @param CreateMasterWaybill $Model The new ContainerCollection resource
     *
     * @return CreateMasterWaybill
     */
    public function create_master_waybillCollection(CreateMasterWaybill $Model): CreateMasterWaybill
    {
        return $this->request(
        'create_master_waybillContainerCollectionCollection',
        'POST',
        'api/shipments/container_collections/create_master_waybill',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a ContainerCollection resource.
     *
     * @param string $id Resource identifier
     *
     * @return ContainerCollectionOutput|null
     */
    public function getItem(string $id): ?ContainerCollectionOutput
    {
        return $this->request(
        'getContainerCollectionItem',
        'GET',
        "api/shipments/container_collections/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the ContainerCollection resource.
     *
     * @param string                    $id    Resource identifier
     * @param ContainerCollectionUpdate $Model The updated ContainerCollection resource
     *
     * @return ContainerCollectionUpdate
     */
    public function putItem(string $id, ContainerCollectionUpdate $Model): ContainerCollectionUpdate
    {
        return $this->request(
        'putContainerCollectionItem',
        'PUT',
        "api/shipments/container_collections/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }
}
