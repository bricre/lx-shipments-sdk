<?php

namespace LogisticsX\Shipments\Api;

use LogisticsX\Shipments\Model\BillOfLadingDelivery\BillOfLadingDeliveryInput;
use LogisticsX\Shipments\Model\BillOfLadingDelivery\BillOfLadingDeliveryOutput;

class BillOfLadingDelivery extends AbstractAPI
{
    /**
     * Retrieves the collection of BillOfLadingDelivery resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'id'	integer
     *                       'id[]'	array
     *                       'uuid'	string
     *                       'deliveryReference'	string
     *                       'order[id]'	string
     *                       'order[uuid]'	string
     *                       'order[deliveryReference]'	string
     *
     * @return BillOfLadingDeliveryOutput[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getBillOfLadingDeliveryCollection',
        'GET',
        'api/shipments/bill_of_lading_deliveries',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a BillOfLadingDelivery resource.
     *
     * @param BillOfLadingDeliveryInput $Model The new BillOfLadingDelivery resource
     *
     * @return BillOfLadingDeliveryOutput
     */
    public function postCollection(BillOfLadingDeliveryInput $Model): BillOfLadingDeliveryOutput
    {
        return $this->request(
        'postBillOfLadingDeliveryCollection',
        'POST',
        'api/shipments/bill_of_lading_deliveries',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a BillOfLadingDelivery resource.
     *
     * @param string $id Resource identifier
     *
     * @return BillOfLadingDeliveryOutput|null
     */
    public function getItem(string $id): ?BillOfLadingDeliveryOutput
    {
        return $this->request(
        'getBillOfLadingDeliveryItem',
        'GET',
        "api/shipments/bill_of_lading_deliveries/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the BillOfLadingDelivery resource.
     *
     * @param string                    $id    Resource identifier
     * @param BillOfLadingDeliveryInput $Model The updated BillOfLadingDelivery
     *                                         resource
     *
     * @return BillOfLadingDeliveryOutput
     */
    public function putItem(string $id, BillOfLadingDeliveryInput $Model): BillOfLadingDeliveryOutput
    {
        return $this->request(
        'putBillOfLadingDeliveryItem',
        'PUT',
        "api/shipments/bill_of_lading_deliveries/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the BillOfLadingDelivery resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteBillOfLadingDeliveryItem',
        'DELETE',
        "api/shipments/bill_of_lading_deliveries/$id",
        null,
        [],
        []
        );
    }

    /**
     * Creates a BillOfLadingDelivery resource.
     *
     * @param array $data
     *
     * @return BillOfLadingDeliveryOutput
     */
    public function create_deliveryCollection(array $data = []): BillOfLadingDeliveryOutput
    {
        return $this->request(
        'create_deliveryBillOfLadingDeliveryCollection',
        'POST',
        'api/shipments/bill_of_ladings/create_delivery',
        $data,
        [],
        []
        );
    }
}
