<?php

namespace LogisticsX\Shipments\Api;

use LogisticsX\Shipments\Model\ConsignmentCartonProductCollection\ConsignmentCartonProductCollectionOutput;

class ConsignmentCartonProductCollection extends AbstractAPI
{
    /**
     * Creates a ConsignmentCartonProductCollection resource.
     *
     * @return ConsignmentCartonProductCollectionOutput
     */
    public function postCollection(): ConsignmentCartonProductCollectionOutput
    {
        return $this->request(
        'postConsignmentCartonProductCollectionCollection',
        'POST',
        'api/shipments/consignment_carton_product_collections',
        null,
        [],
        []
        );
    }

    /**
     * Retrieves a ConsignmentCartonProductCollection resource.
     *
     * @param string $id Resource identifier
     *
     * @return ConsignmentCartonProductCollectionOutput|null
     */
    public function getItem(string $id): ?ConsignmentCartonProductCollectionOutput
    {
        return $this->request(
        'getConsignmentCartonProductCollectionItem',
        'GET',
        "api/shipments/consignment_carton_product_collections/$id",
        null,
        [],
        []
        );
    }
}
