<?php

namespace LogisticsX\Shipments\Api;

use LogisticsX\Shipments\Model\Container\ContainerCollection;
use LogisticsX\Shipments\Model\Container\ContainerInput;
use LogisticsX\Shipments\Model\Container\ContainerOutput;
use LogisticsX\Shipments\Model\Container\Statistics;

class Container extends AbstractAPI
{
    /**
     * Retrieves the collection of Container resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'uuid'	string
     *                       'uuid[]'	string
     *                       'masterWaybill.uuid'	string
     *                       'masterWaybill.uuid[]'	string
     *                       'billOfLadings.uuid'	string
     *                       'billOfLadings.uuid[]'	string
     *                       'clientCode'	string
     *                       'status'	string
     *                       'status[]'	array
     *                       'reference'	string
     *                       'masterWaybill.reference'	string
     *                       'size'	string
     *                       'size[]'	array
     *                       'masterWaybill.placeOfDepartureLocationCode'	string
     *                       'masterWaybill.placeOfDepartureLocationName'	string
     *                       'masterWaybill.placeOfDeliveryLocationCode'	string
     *                       'masterWaybill.placeOfDeliveryLocationName'	string
     *                       'masterWaybill.meansOfTransportCode'	string
     *                       'deliveryType'	string
     *                       'deliveryType[]'	array
     *                       'masterWaybill.estimatedDateOfArrival[before]'	string
     *                       'masterWaybill.estimatedDateOfArrival[strictly_before]'	string
     *                       'masterWaybill.estimatedDateOfArrival[after]'	string
     *                       'masterWaybill.estimatedDateOfArrival[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[uuid]'	string
     *                       'order[createTime]'	string
     *                       'order[updateTime]'	string
     *                       'order[masterWaybill.estimatedDateOfArrival]'	string
     *                       'order[masterWaybill.actualDateOfArrival]'	string
     *                       'order[scheduledCollectionDate]'	string
     *                       'order[releaseCodeExpiryTime]'	string
     *                       'order[status]'	string
     *
     * @return ContainerCollection[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getContainerCollection',
        'GET',
        'api/shipments/containers',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a Container resource.
     *
     * @param ContainerInput $Model The new Container resource
     *
     * @return ContainerOutput
     */
    public function postCollection(ContainerInput $Model): ContainerOutput
    {
        return $this->request(
        'postContainerCollection',
        'POST',
        'api/shipments/containers',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves the collection of Container resources.
     *
     * @param string $billOfLadingUuid
     * @param array  $queries          options:
     *                                 'page'	integer	The collection page number
     *                                 'itemsPerPage'	integer	The number of items per page
     *                                 'uuid'	string
     *                                 'uuid[]'	string
     *                                 'masterWaybill.uuid'	string
     *                                 'masterWaybill.uuid[]'	string
     *                                 'billOfLadings.uuid'	string
     *                                 'billOfLadings.uuid[]'	string
     *                                 'clientCode'	string
     *                                 'status'	string
     *                                 'status[]'	array
     *                                 'reference'	string
     *                                 'masterWaybill.reference'	string
     *                                 'size'	string
     *                                 'size[]'	array
     *                                 'masterWaybill.placeOfDepartureLocationCode'	string
     *                                 'masterWaybill.placeOfDepartureLocationName'	string
     *                                 'masterWaybill.placeOfDeliveryLocationCode'	string
     *                                 'masterWaybill.placeOfDeliveryLocationName'	string
     *                                 'masterWaybill.meansOfTransportCode'	string
     *                                 'deliveryType'	string
     *                                 'deliveryType[]'	array
     *                                 'masterWaybill.estimatedDateOfArrival[before]'	string
     *                                 'masterWaybill.estimatedDateOfArrival[strictly_before]'	string
     *                                 'masterWaybill.estimatedDateOfArrival[after]'	string
     *                                 'masterWaybill.estimatedDateOfArrival[strictly_after]'	string
     *                                 'order[id]'	string
     *                                 'order[uuid]'	string
     *                                 'order[createTime]'	string
     *                                 'order[updateTime]'	string
     *                                 'order[masterWaybill.estimatedDateOfArrival]'	string
     *                                 'order[masterWaybill.actualDateOfArrival]'	string
     *                                 'order[scheduledCollectionDate]'	string
     *                                 'order[releaseCodeExpiryTime]'	string
     *                                 'order[status]'	string
     *
     * @return ContainerOutput[]|null
     */
    public function getLinkableByBillOfLadingCollection(string $billOfLadingUuid, array $queries = []): ?array
    {
        return $this->request(
        'getLinkableByBillOfLadingContainerCollection',
        'GET',
        "api/shipments/containers/get_linkable_by_bill_of_lading/$billOfLadingUuid",
        null,
        $queries,
        []
        );
    }

    /**
     * Retrieves the collection of Container resources.
     *
     * @param string $consignmentUuid
     * @param array  $queries         options:
     *                                'page'	integer	The collection page number
     *                                'itemsPerPage'	integer	The number of items per page
     *                                'uuid'	string
     *                                'uuid[]'	string
     *                                'masterWaybill.uuid'	string
     *                                'masterWaybill.uuid[]'	string
     *                                'billOfLadings.uuid'	string
     *                                'billOfLadings.uuid[]'	string
     *                                'clientCode'	string
     *                                'status'	string
     *                                'status[]'	array
     *                                'reference'	string
     *                                'masterWaybill.reference'	string
     *                                'size'	string
     *                                'size[]'	array
     *                                'masterWaybill.placeOfDepartureLocationCode'	string
     *                                'masterWaybill.placeOfDepartureLocationName'	string
     *                                'masterWaybill.placeOfDeliveryLocationCode'	string
     *                                'masterWaybill.placeOfDeliveryLocationName'	string
     *                                'masterWaybill.meansOfTransportCode'	string
     *                                'deliveryType'	string
     *                                'deliveryType[]'	array
     *                                'masterWaybill.estimatedDateOfArrival[before]'	string
     *                                'masterWaybill.estimatedDateOfArrival[strictly_before]'	string
     *                                'masterWaybill.estimatedDateOfArrival[after]'	string
     *                                'masterWaybill.estimatedDateOfArrival[strictly_after]'	string
     *                                'order[id]'	string
     *                                'order[uuid]'	string
     *                                'order[createTime]'	string
     *                                'order[updateTime]'	string
     *                                'order[masterWaybill.estimatedDateOfArrival]'	string
     *                                'order[masterWaybill.actualDateOfArrival]'	string
     *                                'order[scheduledCollectionDate]'	string
     *                                'order[releaseCodeExpiryTime]'	string
     *                                'order[status]'	string
     *
     * @return ContainerOutput[]|null
     */
    public function getLinkableByConsignmentCollection(string $consignmentUuid, array $queries = []): ?array
    {
        return $this->request(
        'getLinkableByConsignmentContainerCollection',
        'GET',
        "api/shipments/containers/get_linkable_by_consignment/$consignmentUuid",
        null,
        $queries,
        []
        );
    }

    /**
     * Retrieves the collection of Container resources.
     *
     * @param string $masterWaybillUuid
     * @param array  $queries           options:
     *                                  'page'	integer	The collection page number
     *                                  'itemsPerPage'	integer	The number of items per page
     *                                  'uuid'	string
     *                                  'uuid[]'	string
     *                                  'masterWaybill.uuid'	string
     *                                  'masterWaybill.uuid[]'	string
     *                                  'billOfLadings.uuid'	string
     *                                  'billOfLadings.uuid[]'	string
     *                                  'clientCode'	string
     *                                  'status'	string
     *                                  'status[]'	array
     *                                  'reference'	string
     *                                  'masterWaybill.reference'	string
     *                                  'size'	string
     *                                  'size[]'	array
     *                                  'masterWaybill.placeOfDepartureLocationCode'	string
     *                                  'masterWaybill.placeOfDepartureLocationName'	string
     *                                  'masterWaybill.placeOfDeliveryLocationCode'	string
     *                                  'masterWaybill.placeOfDeliveryLocationName'	string
     *                                  'masterWaybill.meansOfTransportCode'	string
     *                                  'deliveryType'	string
     *                                  'deliveryType[]'	array
     *                                  'masterWaybill.estimatedDateOfArrival[before]'	string
     *                                  'masterWaybill.estimatedDateOfArrival[strictly_before]'	string
     *                                  'masterWaybill.estimatedDateOfArrival[after]'	string
     *                                  'masterWaybill.estimatedDateOfArrival[strictly_after]'	string
     *                                  'order[id]'	string
     *                                  'order[uuid]'	string
     *                                  'order[createTime]'	string
     *                                  'order[updateTime]'	string
     *                                  'order[masterWaybill.estimatedDateOfArrival]'	string
     *                                  'order[masterWaybill.actualDateOfArrival]'	string
     *                                  'order[scheduledCollectionDate]'	string
     *                                  'order[releaseCodeExpiryTime]'	string
     *                                  'order[status]'	string
     *
     * @return ContainerOutput[]|null
     */
    public function getLinkableByMasterWaybillCollection(string $masterWaybillUuid, array $queries = []): ?array
    {
        return $this->request(
        'getLinkableByMasterWaybillContainerCollection',
        'GET',
        "api/shipments/containers/get_linkable_by_master_waybill/$masterWaybillUuid",
        null,
        $queries,
        []
        );
    }

    /**
     * Retrieves the collection of Container resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'uuid'	string
     *                       'uuid[]'	string
     *                       'masterWaybill.uuid'	string
     *                       'masterWaybill.uuid[]'	string
     *                       'billOfLadings.uuid'	string
     *                       'billOfLadings.uuid[]'	string
     *                       'clientCode'	string
     *                       'status'	string
     *                       'status[]'	array
     *                       'reference'	string
     *                       'masterWaybill.reference'	string
     *                       'size'	string
     *                       'size[]'	array
     *                       'masterWaybill.placeOfDepartureLocationCode'	string
     *                       'masterWaybill.placeOfDepartureLocationName'	string
     *                       'masterWaybill.placeOfDeliveryLocationCode'	string
     *                       'masterWaybill.placeOfDeliveryLocationName'	string
     *                       'masterWaybill.meansOfTransportCode'	string
     *                       'deliveryType'	string
     *                       'deliveryType[]'	array
     *                       'masterWaybill.estimatedDateOfArrival[before]'	string
     *                       'masterWaybill.estimatedDateOfArrival[strictly_before]'	string
     *                       'masterWaybill.estimatedDateOfArrival[after]'	string
     *                       'masterWaybill.estimatedDateOfArrival[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[uuid]'	string
     *                       'order[createTime]'	string
     *                       'order[updateTime]'	string
     *                       'order[masterWaybill.estimatedDateOfArrival]'	string
     *                       'order[masterWaybill.actualDateOfArrival]'	string
     *                       'order[scheduledCollectionDate]'	string
     *                       'order[releaseCodeExpiryTime]'	string
     *                       'order[status]'	string
     *
     * @return Statistics[]|null
     */
    public function statusStatisticsCollection(array $queries = []): ?array
    {
        return $this->request(
        'statusStatisticsContainerCollection',
        'GET',
        'api/shipments/containers/statistics',
        null,
        $queries,
        []
        );
    }

    /**
     * Retrieves a Container resource.
     *
     * @param string $uuid Resource identifier
     *
     * @return ContainerOutput|null
     */
    public function getItem(string $uuid): ?ContainerOutput
    {
        return $this->request(
        'getContainerItem',
        'GET',
        "api/shipments/containers/$uuid",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the Container resource.
     *
     * @param string         $uuid  Resource identifier
     * @param ContainerInput $Model The updated Container resource
     *
     * @return ContainerOutput
     */
    public function putItem(string $uuid, ContainerInput $Model): ContainerOutput
    {
        return $this->request(
        'putContainerItem',
        'PUT',
        "api/shipments/containers/$uuid",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the Container resource.
     *
     * @param string $uuid Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $uuid): mixed
    {
        return $this->request(
        'deleteContainerItem',
        'DELETE',
        "api/shipments/containers/$uuid",
        null,
        [],
        []
        );
    }

    /**
     * Retrieves a Container resource.
     *
     * @param string $uuid Resource identifier
     */
    public function download_unpacking_listItem(string $uuid)
    {
        return $this->request(
        'download_unpacking_listContainerItem',
        'GET',
        "api/shipments/containers/$uuid/unpacking_list.{format}",
        null,
        [],
        []
        );
    }

    /**
     * Retrieves a MasterWaybill resource.
     *
     * @param string $uuid    MasterWaybill identifier
     * @param array  $queries options:
     *                        'page'	integer	The collection page number
     *                        'itemsPerPage'	integer	The number of items per page
     *                        'uuid'	string
     *                        'uuid[]'	string
     *                        'masterWaybill.uuid'	string
     *                        'masterWaybill.uuid[]'	string
     *                        'billOfLadings.uuid'	string
     *                        'billOfLadings.uuid[]'	string
     *                        'clientCode'	string
     *                        'status'	string
     *                        'status[]'	array
     *                        'reference'	string
     *                        'masterWaybill.reference'	string
     *                        'size'	string
     *                        'size[]'	array
     *                        'masterWaybill.placeOfDepartureLocationCode'	string
     *                        'masterWaybill.placeOfDepartureLocationName'	string
     *                        'masterWaybill.placeOfDeliveryLocationCode'	string
     *                        'masterWaybill.placeOfDeliveryLocationName'	string
     *                        'masterWaybill.meansOfTransportCode'	string
     *                        'deliveryType'	string
     *                        'deliveryType[]'	array
     *                        'masterWaybill.estimatedDateOfArrival[before]'	string
     *                        'masterWaybill.estimatedDateOfArrival[strictly_before]'	string
     *                        'masterWaybill.estimatedDateOfArrival[after]'	string
     *                        'masterWaybill.estimatedDateOfArrival[strictly_after]'	string
     *                        'order[id]'	string
     *                        'order[uuid]'	string
     *                        'order[createTime]'	string
     *                        'order[updateTime]'	string
     *                        'order[masterWaybill.estimatedDateOfArrival]'	string
     *                        'order[masterWaybill.actualDateOfArrival]'	string
     *                        'order[scheduledCollectionDate]'	string
     *                        'order[releaseCodeExpiryTime]'	string
     *                        'order[status]'	string
     *
     * @return ContainerOutput[]|null
     */
    public function api_master_waybills_containers_get_subresourceMasterWaybillSubresource(string $uuid, array $queries = []): ?array
    {
        return $this->request(
        'api_master_waybills_containers_get_subresourceMasterWaybillSubresource',
        'GET',
        "api/shipments/master_waybills/$uuid/containers",
        null,
        $queries,
        []
        );
    }
}
