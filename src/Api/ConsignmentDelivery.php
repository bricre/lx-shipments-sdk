<?php

namespace LogisticsX\Shipments\Api;

use LogisticsX\Shipments\Model\ConsignmentDelivery\ConsignmentDeliveryInput;
use LogisticsX\Shipments\Model\ConsignmentDelivery\ConsignmentDeliveryOutput;

class ConsignmentDelivery extends AbstractAPI
{
    /**
     * Retrieves the collection of ConsignmentDelivery resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'id'	integer
     *                       'id[]'	array
     *                       'uuid'	string
     *                       'consignment.uuid'	string
     *                       'deliveryReference'	string
     *                       'order[id]'	string
     *                       'order[uuid]'	string
     *                       'order[consignment.uuid]'	string
     *                       'order[deliveryReference]'	string
     *
     * @return ConsignmentDeliveryOutput[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getConsignmentDeliveryCollection',
        'GET',
        'api/shipments/consignment_deliveries',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a ConsignmentDelivery resource.
     *
     * @param ConsignmentDeliveryInput $Model The new ConsignmentDelivery resource
     *
     * @return ConsignmentDeliveryOutput
     */
    public function postCollection(ConsignmentDeliveryInput $Model): ConsignmentDeliveryOutput
    {
        return $this->request(
        'postConsignmentDeliveryCollection',
        'POST',
        'api/shipments/consignment_deliveries',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a ConsignmentDelivery resource.
     *
     * @param string $uuid Resource identifier
     *
     * @return ConsignmentDeliveryOutput|null
     */
    public function getItem(string $uuid): ?ConsignmentDeliveryOutput
    {
        return $this->request(
        'getConsignmentDeliveryItem',
        'GET',
        "api/shipments/consignment_deliveries/$uuid",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the ConsignmentDelivery resource.
     *
     * @param string                   $uuid  Resource identifier
     * @param ConsignmentDeliveryInput $Model The updated ConsignmentDelivery resource
     *
     * @return ConsignmentDeliveryOutput
     */
    public function putItem(string $uuid, ConsignmentDeliveryInput $Model): ConsignmentDeliveryOutput
    {
        return $this->request(
        'putConsignmentDeliveryItem',
        'PUT',
        "api/shipments/consignment_deliveries/$uuid",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the ConsignmentDelivery resource.
     *
     * @param string $uuid Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $uuid): mixed
    {
        return $this->request(
        'deleteConsignmentDeliveryItem',
        'DELETE',
        "api/shipments/consignment_deliveries/$uuid",
        null,
        [],
        []
        );
    }

    /**
     * Creates a ConsignmentDelivery resource.
     *
     * @param array $data
     *
     * @return ConsignmentDeliveryOutput
     */
    public function create_deliveryCollection(array $data = []): ConsignmentDeliveryOutput
    {
        return $this->request(
        'create_deliveryConsignmentDeliveryCollection',
        'POST',
        'api/shipments/create_delivery',
        $data,
        [],
        []
        );
    }
}
