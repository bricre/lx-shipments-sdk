<?php

namespace LogisticsX\Shipments;

use OpenAPI\Runtime\ResponseTypes as AbstractResponseTypes;

class ResponseTypes extends AbstractResponseTypes
{
    public array $types = [
        'postBillOfLadingCollectionCollection' => [
            '201.' => 'LogisticsX\\Shipments\\Model\\BillOfLadingCollection\\BillOfLadingCollectionOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'create_containerBillOfLadingCollectionCollection' => [
            '201.' => 'LogisticsX\\Shipments\\Model\\BillOfLadingCollection\\BillOfLadingCollection\\CreateContainer',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'create_master_waybillBillOfLadingCollectionCollection' => [
            '201.' => 'LogisticsX\\Shipments\\Model\\BillOfLadingCollection\\BillOfLadingCollection\\CreateMasterWaybill',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getBillOfLadingCollectionItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\BillOfLadingCollection\\BillOfLadingCollectionOutput',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putBillOfLadingCollectionItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\BillOfLadingCollection\\BillOfLadingCollectionUpdate',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getBillOfLadingDeliveryCollection' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\BillOfLadingDelivery\\BillOfLadingDeliveryOutput[]',
        ],
        'postBillOfLadingDeliveryCollection' => [
            '201.' => 'LogisticsX\\Shipments\\Model\\BillOfLadingDelivery\\BillOfLadingDeliveryOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getBillOfLadingDeliveryItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\BillOfLadingDelivery\\BillOfLadingDeliveryOutput',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putBillOfLadingDeliveryItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\BillOfLadingDelivery\\BillOfLadingDeliveryOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteBillOfLadingDeliveryItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getBillOfLadingCollection' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\BillOfLading\\BillOfLadingCollection[]',
        ],
        'postBillOfLadingCollection' => [
            '201.' => 'LogisticsX\\Shipments\\Model\\BillOfLading\\BillOfLadingOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'create_deliveryBillOfLadingDeliveryCollection' => [
            '201.' => 'LogisticsX\\Shipments\\Model\\BillOfLadingDelivery\\BillOfLadingDeliveryOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getLinkableByConsignmentBillOfLadingCollection' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\BillOfLading\\BillOfLadingOutput[]',
        ],
        'getLinkableByContainerBillOfLadingCollection' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\BillOfLading\\BillOfLadingOutput[]',
        ],
        'getLinkableByMasterWaybillBillOfLadingCollection' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\BillOfLading\\BillOfLadingOutput[]',
        ],
        'statusStatisticsBillOfLadingCollection' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\BillOfLading\\Statistics[]',
        ],
        'getBillOfLadingItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\BillOfLading\\BillOfLadingOutput',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putBillOfLadingItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\BillOfLading\\BillOfLadingOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteBillOfLadingItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'postConsignmentCartonCollectionCollection' => [
            '201.' => 'LogisticsX\\Shipments\\Model\\ConsignmentCartonCollection\\ConsignmentCartonCollectionOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getConsignmentCartonCollectionItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\ConsignmentCartonCollection\\ConsignmentCartonCollectionOutput',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'postConsignmentCartonProductCollectionCollection' => [
            '201.' => 'LogisticsX\\Shipments\\Model\\ConsignmentCartonProductCollection\\ConsignmentCartonProductCollectionOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getConsignmentCartonProductCollectionItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\ConsignmentCartonProductCollection\\ConsignmentCartonProductCollectionOutput',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getConsignmentCartonProductCollection' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\ConsignmentCartonProduct\\ConsignmentCartonProductOutput[]',
        ],
        'postConsignmentCartonProductCollection' => [
            '201.' => 'LogisticsX\\Shipments\\Model\\ConsignmentCartonProduct\\ConsignmentCartonProductOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'summaryByConsignmentConsignmentCartonProductCollection' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\ConsignmentCartonProduct\\ConsignmentCartonProduct\\SummaryByConsignment[]',
        ],
        'getConsignmentCartonProductItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\ConsignmentCartonProduct\\ConsignmentCartonProductOutput',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putConsignmentCartonProductItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\ConsignmentCartonProduct\\ConsignmentCartonProductOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteConsignmentCartonProductItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getConsignmentCartonCollection' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\ConsignmentCarton\\ConsignmentCartonOutput[]',
        ],
        'postConsignmentCartonCollection' => [
            '201.' => 'LogisticsX\\Shipments\\Model\\ConsignmentCarton\\ConsignmentCartonOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getConsignmentCartonItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\ConsignmentCarton\\ConsignmentCartonOutput',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putConsignmentCartonItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\ConsignmentCarton\\ConsignmentCartonOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteConsignmentCartonItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'postConsignmentCollectionCollection' => [
            '201.' => 'LogisticsX\\Shipments\\Model\\ConsignmentCollection\\ConsignmentCollectionOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'create_bill_of_ladingConsignmentCollectionCollection' => [
            '201.' => 'LogisticsX\\Shipments\\Model\\ConsignmentCollection\\ConsignmentCollection\\CreateBillOfLading',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getConsignmentCollectionItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\ConsignmentCollection\\ConsignmentCollectionOutput',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putConsignmentCollectionItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\ConsignmentCollection\\ConsignmentCollectionUpdate',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getConsignmentDeliveryCollection' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\ConsignmentDelivery\\ConsignmentDeliveryOutput[]',
        ],
        'postConsignmentDeliveryCollection' => [
            '201.' => 'LogisticsX\\Shipments\\Model\\ConsignmentDelivery\\ConsignmentDeliveryOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getConsignmentDeliveryItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\ConsignmentDelivery\\ConsignmentDeliveryOutput',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putConsignmentDeliveryItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\ConsignmentDelivery\\ConsignmentDeliveryOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteConsignmentDeliveryItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getConsignmentDeliveryErrorCollection' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\ConsignmentDeliveryError\\ConsignmentDeliveryErrorInput[]',
        ],
        'postConsignmentDeliveryErrorCollection' => [
            '201.' => 'LogisticsX\\Shipments\\Model\\ConsignmentDeliveryError\\ConsignmentDeliveryErrorInput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getConsignmentDeliveryErrorItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\ConsignmentDeliveryError\\ConsignmentDeliveryErrorInput',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getConsignmentFeeCollection' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\ConsignmentFee\\ConsignmentFeeOutput[]',
        ],
        'postConsignmentFeeCollection' => [
            '201.' => 'LogisticsX\\Shipments\\Model\\ConsignmentFee\\ConsignmentFeeOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getConsignmentFeeItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\ConsignmentFee\\ConsignmentFeeOutput',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putConsignmentFeeItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\ConsignmentFee\\ConsignmentFeeOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteConsignmentFeeItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getConsignmentCollection' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\Consignment\\ConsignmentCollection[]',
        ],
        'postConsignmentCollection' => [
            '201.' => 'LogisticsX\\Shipments\\Model\\Consignment\\ConsignmentOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getLinkableByBillOfLadingConsignmentCollection' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\Consignment\\ConsignmentOutput[]',
        ],
        'getLinkableByMasterWaybillConsignmentCollection' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\Consignment\\ConsignmentOutput[]',
        ],
        'statusStatisticsConsignmentCollection' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\Consignment\\Statistics[]',
        ],
        'uploadConsignmentCollection' => [
            '201.' => 'LogisticsX\\Shipments\\Model\\Consignment\\ConsignmentOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getConsignmentItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\Consignment\\ConsignmentOutput',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putConsignmentItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\Consignment\\ConsignmentOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteConsignmentItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'create_master_waybillContainerCollectionCollection' => [
            '201.' => 'LogisticsX\\Shipments\\Model\\ContainerCollection\\ContainerCollection\\CreateMasterWaybill',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getContainerCollectionItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\ContainerCollection\\ContainerCollectionOutput',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putContainerCollectionItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\ContainerCollection\\ContainerCollectionUpdate',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getContainerCollection' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\Container\\ContainerCollection[]',
        ],
        'postContainerCollection' => [
            '201.' => 'LogisticsX\\Shipments\\Model\\Container\\ContainerOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getLinkableByBillOfLadingContainerCollection' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\Container\\ContainerOutput[]',
        ],
        'getLinkableByConsignmentContainerCollection' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\Container\\ContainerOutput[]',
        ],
        'getLinkableByMasterWaybillContainerCollection' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\Container\\ContainerOutput[]',
        ],
        'statusStatisticsContainerCollection' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\Container\\Statistics[]',
        ],
        'getContainerItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\Container\\ContainerOutput',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putContainerItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\Container\\ContainerOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteContainerItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'download_unpacking_listContainerItem' => [
            '200.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'create_deliveryConsignmentDeliveryCollection' => [
            '201.' => 'LogisticsX\\Shipments\\Model\\ConsignmentDelivery\\ConsignmentDeliveryOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getCustomsBrokerCollection' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\CustomsBroker\\CustomsBrokerInput[]',
        ],
        'postCustomsBrokerCollection' => [
            '201.' => 'LogisticsX\\Shipments\\Model\\CustomsBroker\\CustomsBrokerInput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getCustomsBrokerItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\CustomsBroker\\CustomsBrokerInput',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putCustomsBrokerItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\CustomsBroker\\CustomsBrokerInput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteCustomsBrokerItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getDeliveryServiceCollection' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\DeliveryService\\DeliveryServiceRead[]',
        ],
        'getDeliveryServiceItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\DeliveryService\\DeliveryServiceRead',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'postMasterWaybillCollectionCollection' => [
            '201.' => 'LogisticsX\\Shipments\\Model\\MasterWaybillCollection\\MasterWaybillCollectionOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getMasterWaybillCollectionItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\MasterWaybillCollection\\MasterWaybillCollectionOutput',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putMasterWaybillCollectionItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\MasterWaybillCollection\\MasterWaybillCollectionUpdate',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getMasterWaybillCollection' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\MasterWaybill\\MasterWaybillOutput[]',
        ],
        'postMasterWaybillCollection' => [
            '201.' => 'LogisticsX\\Shipments\\Model\\MasterWaybill\\MasterWaybillOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getLinkableByBillOfLadingMasterWaybillCollection' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\MasterWaybill\\MasterWaybillOutput[]',
        ],
        'getLinkableByConsignmentMasterWaybillCollection' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\MasterWaybill\\MasterWaybillOutput[]',
        ],
        'statusStatisticsMasterWaybillCollection' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\MasterWaybill\\Statistics[]',
        ],
        'getMasterWaybillItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\MasterWaybill\\MasterWaybillOutput',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putMasterWaybillItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\MasterWaybill\\MasterWaybillOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteMasterWaybillItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'api_master_waybills_containers_get_subresourceMasterWaybillSubresource' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\Container\\ContainerOutput[]',
        ],
        'customsClearanceConsignmentCollection' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\Consignment\\Customs\\Clearance[]',
        ],
        'stockInConsignmentCollection' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\Consignment\\Stock\\In[]',
        ],
        'getOutStockOrderCollection' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\OutStockOrder\\OutStockOrderOutput[]',
        ],
        'postOutStockOrderCollection' => [
            '201.' => 'LogisticsX\\Shipments\\Model\\OutStockOrder\\OutStockOrderOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'create_deliveryOutStockOrderCollection' => [
            '201.' => 'LogisticsX\\Shipments\\Model\\OutStockOrder\\OutStockOrderOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getOutStockOrderItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\OutStockOrder\\OutStockOrderOutput',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putOutStockOrderItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\OutStockOrder\\OutStockOrderOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteOutStockOrderItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getProductSpecialAttributeCollection' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\ProductSpecialAttribute\\ProductSpecialAttributeOutput[]',
        ],
        'postProductSpecialAttributeCollection' => [
            '201.' => 'LogisticsX\\Shipments\\Model\\ProductSpecialAttribute\\ProductSpecialAttributeOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getProductSpecialAttributeItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\ProductSpecialAttribute\\ProductSpecialAttributeOutput',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putProductSpecialAttributeItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\ProductSpecialAttribute\\ProductSpecialAttributeOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteProductSpecialAttributeItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getShipmentFeeTypeCollection' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\ShipmentFeeType\\ShipmentFeeTypeOutput[]',
        ],
        'postShipmentFeeTypeCollection' => [
            '201.' => 'LogisticsX\\Shipments\\Model\\ShipmentFeeType\\ShipmentFeeTypeOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getShipmentFeeTypeItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\ShipmentFeeType\\ShipmentFeeTypeOutput',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteShipmentFeeTypeItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getTransitPalletCollection' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\TransitPallet\\TransitPalletCollection[]',
        ],
        'postTransitPalletCollection' => [
            '201.' => 'LogisticsX\\Shipments\\Model\\TransitPallet',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getTransitPalletItem' => [
            '200.' => 'LogisticsX\\Shipments\\Model\\TransitPallet\\TransitPalletOutput',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteTransitPalletItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
    ];
}
